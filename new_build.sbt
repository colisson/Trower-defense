val swing = "org.scala-lang" % "scala-swing" % "2.10+"
val actor = "org.scala-lang" % "scala-actors" % "2.10+"

lazy val root = (project in file(".")).
  settings(
    name := "Trower-defense",
    libraryDependencies += swing,
    libraryDependencies += actor
  )
