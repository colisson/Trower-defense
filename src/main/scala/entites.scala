package kokarde_defense

// ========================================
// ==============  Objets  ================
// ========================================
// Les classes définies dans ce fichier sont
// utiles ensuite pour définir les classes des
// fichiers etudiants, projectiles et membres_bde.
// Ils définissent les objets mobiles et/ou que
// l'on peut poser sur le plateau

import kokarde._
// Les Map sont une bonne alternative entre les listes
// (que l'on peut mapper) et les tableaux (accessibilité
// en temps constant)
import scala.collection.mutable._

abstract class Nameable {
  // Le nom est utilisé pour afficher l'image.
  // Il doit être de la forme <type_objet>_<nom_objet>
  // Ex : tour_ultrapuissante
  //      monstre_quifaitpeur
  // L'image en question doit être carré et se trouver
  // dans src/main/resources/georges/
  // Pretty_nom en revanche est utilisé pour afficher un
  // beau nom à l'utilisateur.
  protected var m_nom : String
  protected var m_pretty_nom : String
  // -------- Getter --------
  def get_nom = {m_nom}
  def get_pretty_nom = {m_pretty_nom}
}

// Cette classe est utilisée pour créer de nouveaux objets sans les
// cloner. Cette usine permet de créer des objets via objet_usine(i,j)
abstract class Usine_ij {
  def apply(coord:(Int,Int)) : Entite
  // Le nom est utilisé pour afficher l'image.
  // Il doit être de la forme <type_objet>_<nom_objet>
  // Ex : tour_ultrapuissante
  //      monstre_quifaitpeur
  // L'image en question doit être carré et se trouver
  // dans src/main/resources/georges/
  // Pretty_nom en revanche est utilisé pour afficher un
  // beau nom à l'utilisateur.
  protected var m_nom = ""
  protected var m_pretty_nom = ""
  // -------- Setter/Getter --------
  def get_nom = {m_nom}
  def set_nom(n:String) {m_nom = n}

  def get_pretty_nom = {m_pretty_nom}
  def set_pretty_nom(n:String) {m_pretty_nom = n}
}
// Classe générale des objets
abstract class Entite(coord:(Int,Int)) {
  protected var m_pos_i:Int = coord._1
  protected var m_pos_j:Int = coord._2

  // -------- Setter/Getter --------
  def get_pos_i = {m_pos_i}
  def set_pos_i(i:Int) {m_pos_i=i}

  def get_pos_j = {m_pos_j}
  def set_pos_j(j:Int) {m_pos_j=j}

  def get_pos_ij = {(m_pos_i, m_pos_j)}
  def get_coord = get_pos_ij
  def set_pos_ij(c:(Int,Int)){c match{case (i,j) => m_pos_i = i;m_pos_j = j}}
  def set_coord(c:(Int,Int)) { set_pos_ij(c) }
}

// Pour obtenir un mouvement fluide des objets on définit leur position
// future en plus puis on fera une interpolation linéaire entre les
// deux positions. nb_steps is the number of steps to do to move from
// one case to the other one.
abstract class Movable(nb_steps:Int, coord:(Int,Int), next_coord:(Int,Int)) extends Entite(coord)
{
  protected var m_next_pos_i = next_coord._1
  protected var m_next_pos_j = next_coord._2
  protected var m_nb_mini_steps = nb_steps
  protected var m_current_mini_steps = 0
  // -------- Gérer pos --------
  def get_next_pos_i = {m_next_pos_i}
  def set_next_pos_i(i:Int) {m_next_pos_i = i}
  def get_next_pos_j = {m_next_pos_j}
  def set_next_pos_j(j:Int) {m_next_pos_j = j}
  def get_next_pos_ij = {(m_next_pos_i, m_next_pos_j)}
  def get_next_coord = {get_next_pos_ij}
  def set_next_pos_ij(c:(Int,Int)) {
    c match {case (i,j) => m_next_pos_i = i; m_next_pos_j = j}}
  def set_next_coord(c:(Int,Int)) {set_next_pos_ij(c)}
  // -------- Gérer nb_steps --------
  def get_nb_mini_steps = {m_nb_mini_steps}
  def set_nb_mini_steps(n:Int) {m_nb_mini_steps = n}
  def get_current_mini_steps = {m_current_mini_steps}
  def set_current_mini_steps(n:Int) {m_current_mini_steps = n}
  // -------- Gérer avancement --------
  // Chaque élément doit définir une méthode bouger qui met à jour
  // le nouveau but, et effectue des actions déterminées quand
  // un étudiant atteint son but.
  def bouger_grand_pas()
  def delta_mini_steps() : Int
  def changer_etat_mini_step()
  def avancer_mini_step {
    m_current_mini_steps += delta_mini_steps
    changer_etat_mini_step
    if (m_current_mini_steps >= get_nb_mini_steps)
    {
      m_current_mini_steps = 0;
      this.set_pos_ij(this.get_next_pos_ij)
      this.bouger_grand_pas;
    }
  }
}


