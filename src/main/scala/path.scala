package kokarde_defense

// ===================================
// ============  Path  ===============
// ===================================

// La partie sur la pathfinding se fait dans ce fichier. Chaque couple
// (type d'étudiant, but) a une carte de distance associées, et le
// déplacement s'effectue en fonction de cette carte (pour la majorité
// des etudiants, ils choisiront au hasard une case à visiter parmi
// les cases disponibles de distance minimale par rapport au but.  Il
// est également nécessaire de vérifier à chaque étape que l'on ne casse
// pas l'accessibilité du graph.

import scala.collection.mutable._

// Un Path représente la distance à un but donné pour un étudiant
// de type donné
// TODO : optimiser la fonction d'ajout de membre bde pour
// ne modifier que les cases qui doivent être recalculée
class Path(type_etud:Type_etudiant,objectif:But)
{
  // -------- Variables --------
  protected var m_type_etud = type_etud
  protected var nbr_lignes = kokarde.get_nbr_lignes
  protected var nbr_colonnes = kokarde.get_nbr_colonnes
  // Les cases sont traitées les unes après les autres
  // Elles sont toutes placées au départ dans "cases_a_traiter",
  // puis on intialise "cases_en_traitement" au cases de départ
  // en plaçant les cases voisines dans "cases_a_traiter"
  // puis on étend progressivement les cases dont la distance
  // à la case départ a déjà été calculée en les plaçant dans
  // "cases_traitees".
  protected var cases_0: Map[(Int,Int),Int] = kokarde.get_cases_but(objectif)
  protected var cases_traitees: Map[(Int,Int),Int] = Map()
  protected var cases_a_traiter: Map[(Int,Int),Int] = Map()
  protected var cases_en_traitement: Map[(Int,Int),Int] = cases_0
  protected var distance_actuelle = 0
  protected var distance_incr = 10
  protected var valeur_cases = Array.ofDim[Int](nbr_lignes,nbr_colonnes)
  protected var path_old = valeur_cases

  def get_valeur_cases = valeur_cases
  def get_accessibilite = verifie_accessibilite
  def get_old = valeur_cases
  def get_valeur_case(c:(Int,Int)) = {c match {case (i,j) =>
    if (kokarde.case_dans_kokarde(c))
      valeur_cases(i)(j)
    else
      -1
  }}

  protected def est_accessible(c:(Int,Int)):Boolean = {
    (kokarde.est_accessible_absolu(m_type_etud,c,false))
  }

  protected def est_traitee(c:(Int,Int)):Boolean = {cases_traitees contains c}

  protected def nest_pas_construite(c:(Int,Int)) = {
    !kokarde.contient_membre_bde(c)
  }




  protected def traitement = {
    cases_a_traiter = Map()
    cases_en_traitement.foreach{p => p match { case ((i,j),_) =>
      if (kokarde.case_dans_kokarde((i,j)))
      {
        valeur_cases(i)(j) = distance_actuelle
        cases_traitees += p
        cases_a_traiter += ((i+1,j) -> 1)
        cases_a_traiter += ((i-1,j) -> 1)
        cases_a_traiter += ((i,j-1) -> 1)
        cases_a_traiter += ((i,j+1) -> 1)
      }
    }}
    cases_en_traitement = Map()
    distance_actuelle = distance_actuelle + distance_incr
  }





  protected def est_a_traiter(c:((Int,Int),Int)):Boolean = {
    c match {
      case (c,a) => (est_accessible(c) && !(est_traitee(c)) && (nest_pas_construite(c)|| (kokarde.membre_bde_present(c) match { case Some(membre) => membre.get_nom == "membre_bde_bouclette" case None => false})))
    }
  }
  protected def nettoyage_cases_a_traiter(cases:Map[(Int,Int),Int]):(Map[(Int,Int),Int]) = {
    if (cases.isEmpty)
      cases
    else
      cases.filter(est_a_traiter)
  }

  protected def verifie_accessibilite:Boolean = {
    (kokarde.get_cases_depart).forall{case (c,_) => (get_valeur_case(c) >= 0)}
  }

  def cree_path:Boolean = {
    path_old = valeur_cases
    cases_0 = kokarde.get_cases_but(objectif)
    cases_traitees = Map()
    cases_a_traiter = Map()
    cases_en_traitement = cases_0
    distance_actuelle = 0
    distance_incr = 10

    for {
      i <- 0 until nbr_lignes
      j <- 0 until nbr_colonnes
    } valeur_cases(i)(j) = -1

    while(cases_en_traitement != Map())
    {
      traitement
      cases_en_traitement = nettoyage_cases_a_traiter(cases_a_traiter)
    }
    if(!verifie_accessibilite)
    {valeur_cases = path_old
      false}
    else
    {true}
  }
  cree_path


  def cases_suivantes (c:(Int,Int)):List[(Int,Int)] = {
    c match {
      case (x,y) => {
        val cases_possibles = List((x+1,y),(x-1,y),(x,y+1),(x,y-1)).filter(kokarde.case_dans_kokarde)
        val val_voisins = cases_possibles.map{c => (get_valeur_case(c), c)}.filter{ case (v,_) => v != -1 }
        if (val_voisins.isEmpty)
          List()
        else
        {
          val distance_min = val_voisins.map{_._1}.min
          val_voisins.filter{case (v,_) => v == distance_min}.map(_._2)
        }
      }
    }
  }

  def display {
    println("---------------------")
    for (i <- 0 until nbr_lignes)
    {
      for (j <- 0 until nbr_colonnes)
      {
        print(valeur_cases(i)(j))
        print(";")
      }
      println("")
    }
  }
  display
}


object map_path {

  def maj:Boolean={
    var old_map = map_des_paths
    map_des_paths = Map()
    try {
      map_des_paths = cree_map_path(liste_types_etud)
      true
    } catch {
      case ex: IllegalArgumentException => {
        map_des_paths = old_map
        false
      }
    }
  }

  protected var map_des_paths:Map[(Type_etudiant,But),Path] = Map()
  protected var liste_types_etud = Liste_types_etudiant.get_l
  protected var liste_buts = Liste_buts.get_l
  protected def cree_map_path(entree:List[Type_etudiant]):Map[(Type_etudiant,But),Path] = {
    entree match {
      case List() => Map()
      case t::q => {
        def cree_map_path2(entree2:List[But]):Map[(Type_etudiant,But),Path] =
        {
          entree2 match {
            case List() => Map()
            case t2::q2 => { val aux = new Path(t,t2)
              if (aux.cree_path)
              {Map((t,t2)->aux) ++ cree_map_path2(q2)}
              else
              {
                throw new IllegalArgumentException
              }
            }
          }
        }
        cree_map_path2(liste_buts) ++ cree_map_path(q)
      }
    }
  }
  map_des_paths = cree_map_path(liste_types_etud)



  def get_path(t:Type_etudiant,b:But) = { map_des_paths(t,b) }

  def display_paths {
    map_des_paths.foreach{case (_,path) => path.display}
  }


}












