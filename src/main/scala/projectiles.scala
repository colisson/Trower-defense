package kokarde_defense

// ========================================
// ============  Projectile  ==============
// ========================================
// Les étudiants peuvent boire des verres
// Leur alcoolémie dépends de se résistance
// naturelle, ainsi que de la nature du
// verre proposé. Les projectiles sont
// cependants plus généraux que ça puisqu'ils
// peuvent renvoyer l'étudiant de la kokarde
// tout de suite, le tuer, ou le bourer.

import kokarde._
// Les Map sont une bonne alternative entre les listes
// (que l'on peut mapper) et les tableaux (accessibilité
// en temps constant)
import scala.collection.mutable._
import math._


abstract class Projectile_usine extends Nameable {
  def apply(c:(Int,Int),e:Etudiant) : Projectile
}
abstract class Projectile (proj_usine : Projectile_usine,coord:(Int,Int), cible : Etudiant)
    extends Entite(coord)
{
  protected var m_nb_mini_steps = 10
  protected var m_current_mini_steps = 0
  protected val m_proj_usine = proj_usine
  protected var m_a_explose = false
  protected var m_cible = cible
  // -------- Gérer le nom --------
  def get_nom = m_proj_usine.get_nom
  def get_pretty_nom = m_proj_usine.get_pretty_nom
  // -------- Gérer la position --------
  def get_nb_mini_steps = {m_nb_mini_steps}
  def set_nb_mini_steps(n:Int) {m_nb_mini_steps = n}
  def get_current_mini_steps = {m_current_mini_steps}
  def set_current_mini_steps(n:Int) {m_current_mini_steps = n}
  // -------- Gérer la cible --------
  def get_cible = {m_cible}
  def set_cible(e:Etudiant) {m_cible = e}
  def touche_cible()
  // -------- Gérer le mouvement --------
  def a_explose = {m_a_explose}
  def avancer_mini_step {
    m_current_mini_steps += 1
    if (m_current_mini_steps >= m_nb_mini_steps)
    {
      if(!a_explose)
        touche_cible()
      m_a_explose = true
    }
  }

}

// -------- Renvoyer kokarde --------
object proj_Renvoyer_kokarde_usine extends Projectile_usine {
  def apply(coord:(Int,Int),e:Etudiant) = {new Proj_Renvoyer_kokarde(coord,e)}
  var m_nom = "renvoyer_kokarde"
  var m_pretty_nom = "Renvoyer de la kokarde"

}
class Proj_Renvoyer_kokarde (coord:(Int,Int),cible : Etudiant)
    extends Projectile(proj_Renvoyer_kokarde_usine,coord,cible)
{
  def touche_cible()
  {
    this.m_cible.renvoyer_kokarde
  }
} 

// -------- Coup de poing --------
object proj_Coup_de_poing_usine extends Projectile_usine {
  def apply(coord:(Int,Int),e:Etudiant) = {new Proj_Coup_de_poing(coord,e)}
  var m_nom = "proj_coup_de_poing"
  var m_pretty_nom = "Coup de poing"
}
class Proj_Coup_de_poing
  (coord:(Int,Int),cible : Etudiant)
    extends Projectile(proj_Coup_de_poing_usine,coord,cible)
{
  def touche_cible()
  {
    this.m_cible.add_boure(34)
  }
}

// -------- Coup de poing mortel --------
object proj_Coup_de_poing_mortel_usine extends Projectile_usine {
  def apply(coord:(Int,Int),e:Etudiant) = {new Proj_Coup_de_poing_mortel(coord,e)}
  var m_nom = "proj_coup_de_poing_mortel"
  var m_pretty_nom = "Coup de poing mortel"
}
class Proj_Coup_de_poing_mortel
  (coord:(Int,Int),cible : Etudiant)
    extends Projectile(proj_Coup_de_poing_mortel_usine,coord,cible)
{
  def touche_cible()
  {
    this.m_cible.add_boure(72)
  }
}


// -------- Glacon --------
object proj_Glacon_usine extends Projectile_usine {
  def apply(coord:(Int,Int),e:Etudiant) = {
    new Proj_Glacon(coord,e)}
  var m_nom = "proj_glacon"
  var m_pretty_nom = "Glacon"
}
class Proj_Glacon(coord:(Int,Int),cible : Etudiant)
    extends Projectile(proj_Glacon_usine,coord,cible)
{
  def touche_cible()
  {
    this.m_cible.boire_un_verre(5)
  }
}

// -------- Bierre --------
object proj_Bierre_usine extends Projectile_usine {
  def apply(coord:(Int,Int),e:Etudiant) = {new Proj_Bierre(coord,e)}
  var m_nom = "proj_bierre"
  var m_pretty_nom = "Bierre"
}
class Proj_Bierre(coord:(Int,Int),cible : Etudiant)
    extends Projectile(proj_Bierre_usine,coord,cible)
{
  def touche_cible()
  {
    this.m_cible.boire_un_verre(20)
  }
}

// -------- Vodka --------
object proj_Vodka_usine extends Projectile_usine {
  def apply(coord:(Int,Int),e:Etudiant) = {new Proj_Vodka(coord,e)}
  var m_nom = "proj_vodka"
  var m_pretty_nom = "Vodka"
}
class Proj_Vodka(coord:(Int,Int),cible : Etudiant)
    extends Projectile(proj_Vodka_usine,coord,cible)
{
  def touche_cible()
  {
    this.m_cible.boire_un_verre(30)
  }
}

// -------- Whisky --------
object proj_Whisky_usine extends Projectile_usine {
  def apply(coord:(Int,Int),e:Etudiant) = {new Proj_Whisky(coord,e)}
  var m_nom = "proj_whisky"
  var m_pretty_nom = "Whisky"
}
class Proj_Whisky(coord:(Int,Int),cible : Etudiant)
    extends Projectile(proj_Whisky_usine,coord,cible)
{
  def touche_cible()
  {
    this.m_cible.boire_un_verre(40)
  }
}


// -------- Coup de Fatigue --------
// Utile pour ralentir un joueur
object proj_Coup_de_fatigue_usine extends Projectile_usine {
  def apply(coord:(Int,Int),e:Etudiant) = {new Proj_Coup_de_fatigue(coord,e)}
  var m_nom = "proj_coup_de_fatigue"
  var m_pretty_nom = "Coup de fatigue"
}
class Proj_Coup_de_fatigue(coord:(Int,Int),cible : Etudiant)
    extends Projectile(proj_Coup_de_fatigue_usine,coord,cible)
{
  def touche_cible()
  {
    this.m_cible.set_nb_steps_penalite(4,min(this.m_cible.get_default_nb_steps*6,this.m_cible.get_nb_mini_steps*2))
  }
}

// ------------------------
// -------- Bombe  --------
// ------------------------
// On définit ici des projectiles en "dégats de zone"
abstract class Projectile_Bombe_usine extends Projectile_usine {
  //def apply(coord:(Int,Int),e:Etudiant)
  protected var m_sub_proj_usine:Projectile_usine
  protected var m_portee:Double
  def get_portee = {m_portee}
  def get_sub_proj_usine = {m_sub_proj_usine}
  def touche_cible_principale(e:Etudiant)
}
abstract class Proj_Bombe(proj_Bombe_usine:Projectile_Bombe_usine,coord:(Int,Int),cible : Etudiant)
    extends Projectile(proj_Bombe_usine,coord,cible)
{
  def get_portee = {proj_Bombe_usine.get_portee}
  def touche_cible()
  {
    proj_Bombe_usine.touche_cible_principale(cible)
    var cases_accessibles:Map[(Int,Int),Boolean] = Map()
    val big_portee = math.ceil(get_portee).toInt
    for {i <- (cible.get_pos_i - big_portee) to (cible.get_pos_i + big_portee)
      j <- (cible.get_pos_j - big_portee) to (cible.get_pos_j + big_portee)}
    {
      if ((pow(cible.get_pos_i - i,2) + pow(cible.get_pos_j - j,2)).toDouble <= (get_portee * get_portee))
        cases_accessibles += ((i,j) -> true)
    }
    val all_victimes = cases_accessibles.map(x => kokarde
      .etudiant_present(x._1))
      .collect({case Some(n) => n})
      .filter{v => (v.filtrer_non_tirable)}
    all_victimes.foreach { case et =>
      kokarde.ajouter_projectile(proj_Bombe_usine.get_sub_proj_usine(cible.get_coord,et)) }
  }
}

// -------- Bombe communiste  --------
object proj_Cocktail_molotov_eclat_usine extends Projectile_usine {
  def apply(coord:(Int,Int),e:Etudiant) = {new Proj_Cocktail_molotov_eclat(coord,e)}
  var m_nom = "proj_cocktail_molotov_eclat"
  var m_pretty_nom = "Cocktail molotov eclat"
}
class Proj_Cocktail_molotov_eclat(coord:(Int,Int),cible : Etudiant)
    extends Projectile(proj_Cocktail_molotov_eclat_usine,coord,cible)
{
  def touche_cible()
  {
    this.m_cible.add_boure(5)
  }
}

object proj_Cocktail_molotov_usine extends Projectile_Bombe_usine {
  def apply(coord:(Int,Int),e:Etudiant) = {new Proj_Cocktail_molotov(coord,e)}
  var m_nom = "proj_cocktail_molotov"
  var m_pretty_nom = "Cocktail molotov"
  var m_sub_proj_usine : Projectile_usine = proj_Cocktail_molotov_eclat_usine
  var m_portee = 5.
  def touche_cible_principale(e:Etudiant) {
    e.add_boure(30)
  }
}
class Proj_Cocktail_molotov(coord:(Int,Int),cible : Etudiant)
    extends Proj_Bombe(proj_Cocktail_molotov_usine,coord,cible)
{}




// -------- Coup de Coeur --------
// Utile pour ralentir un joueur...
object proj_Coeur_usine extends Projectile_usine {
  def apply(coord:(Int,Int),e:Etudiant) = {new Proj_Coeur(coord,e)}
  var m_nom = "proj_coeur"
  var m_pretty_nom = "La choppe de l'année"
}
class Proj_Coeur(coord:(Int,Int),cible : Etudiant)
    extends Projectile(proj_Coeur_usine,coord,cible)
{
  m_nb_mini_steps = 0
  m_current_mini_steps = 1000
  def touche_cible()
  {
    this.m_cible.set_etat(Etat_bouclette(100))
  }
}




// -------- Coup de Tentacule --------
object proj_Tentacule_usine extends Projectile_usine {
  def apply(coord:(Int,Int),e:Etudiant) = {new Proj_Tentacule(coord,e)}
  var m_nom = "proj_tentacule"
  var m_pretty_nom = "Une tentacule bien visqueuse"
}
class Proj_Tentacule(coord:(Int,Int),cible : Etudiant)
    extends Projectile(proj_Tentacule_usine,coord,cible)
{
  def touche_cible()
  {
    this.m_cible.boire_un_verre(1)
  }
}

