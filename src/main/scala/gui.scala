package kokarde_defense

// http://www.scala-lang.org/api/current/#scala.collection.mutable.HashMap

import swing._
import swing.event._
import javax.swing.ImageIcon
import java.awt.Dimension

import scala.swing._
import scala.swing.BorderPanel.Position._
import java.awt.{ Color, Graphics2D }
import scala.util.Random
import scala.swing.Panel
import javax.swing.ImageIcon
import java.awt.image.BufferedImage
import java.awt.image._
import javax.swing.Icon
import java.awt.Image
import java.io.File
import java.nio.file.{Paths, Files}
import javax.imageio.ImageIO
import java.awt.color.ColorSpace
import scala.collection.mutable.HashMap

// TODO :
// Les sons : java.sun.audio ou task + AudioPlayer

// ----------------------------------------
// ----------------- Timer ----------------
// ----------------------------------------
// The timers are usefull to update the animation of monsters
object timer_non_recursif {
  protected val m_default_delay = 20
  protected val m_timer: javax.swing.Timer = new javax.swing.Timer(1,  Swing.ActionListener(_ => m_action ()))
  protected var m_action : (() => Unit) = {() => ()}
  def start() = m_timer.start()
  def stop() = m_timer.stop()
  def set_delay(n:Int) {m_timer.setDelay(n)}
  set_delay(m_default_delay)
  def get_delay = {m_timer.getDelay}
  def set_action(f : (() => Unit)) {m_action = f}
  def get_default_delay = m_default_delay
}

// ----------------------------------------
// ----------------- Data -----------------
// ----------------------------------------
// Cette strucure sera transportée un peu partout.
// Elle est très utile pour pouvoir passer
// des actions graphiques au president_bde,
// mais également à tous les fichiers qui pourrait
// vouloir parler à l'interface graphique.
class Data(send_message_fct : String => Unit) {
  // protected var m_time_timer = m_default_time_timer
  // Timer
  // def get_default_time_timer = m_default_time_timer
  // def get_time_timer = {m_time_timer}
  // def set_time_timer(t:Int) {m_time_timer = t}
  // Tower currently selected by the user
  protected var m_selected_membre_bde : Option[Membre_bde_usine] = None
  def set_membre_bde_selectionne(membre_bde : Membre_bde_usine) {
    m_selected_membre_bde = Some(membre_bde)
  }
  def get_membre_bde_selectionne = {m_selected_membre_bde}
  def clear_membre_bde_selectionne  {m_selected_membre_bde = None}
  // Case currently selected by the user
  protected var m_case_actuellement_selectionnee : Option[(Int,Int)] = None
  def set_case_actuellement_selectionnee(i:Int,j:Int) {
    m_case_actuellement_selectionnee = Some((i,j))
  }
  def get_case_actuellement_selectionnee = {m_case_actuellement_selectionnee}
  def clear_case_actuellement_selectionnee  {m_case_actuellement_selectionnee = None}
  // Current state
  def get_current_state = {president.get_etat}
  // Send a message, usually in the bottom of the screen
  def send_message(message : String) {
    send_message_fct(message)
  }
  // Update canvas
  protected var m_update_canvas : (Option[() => Unit]) = None
  def define_update_canvas(f_update_canvas : (() => Unit)) {
    m_update_canvas = Some(f_update_canvas)
  }
  def update_canvas {
    m_update_canvas match
    {
      case None => ()
      case Some(f) => f()
    }
  }
  // Update the menu that choose the membres_bde
  protected var m_menu_choose_membres_bde_tmp : FlowPanel = new FlowPanel
  protected var m_menu_choose_membres_bde : FlowPanel = new FlowPanel {
    contents += m_menu_choose_membres_bde_tmp
  }
  def get_menu_choose_membres_bde = {m_menu_choose_membres_bde}
  def update_menu_choose_membres_bde {
    // We remove all contents
    m_menu_choose_membres_bde.contents -= m_menu_choose_membres_bde_tmp
    // Add the elements
    m_menu_choose_membres_bde_tmp = new FlowPanel {
      contents += new Label("Membres BDE disponibles :")
      president.get_membres_bde_constructibles.foreach( m =>
        contents += new Menu_button_membre_bde(Data.this, m, m.get_prix <= dj.get_or)
      )
      contents += new Label {text = "Étape suivante :"}
      contents += new Menu_button_next_step(Data.this)
    }
    m_menu_choose_membres_bde.contents += m_menu_choose_membres_bde_tmp
    // Update
    m_menu_choose_membres_bde.repaint()
    m_menu_choose_membres_bde.revalidate()
  }
  // Update the menu that displays the user informations
  protected val m_niv_label = new Label
  protected val m_vie_label = new Label
  protected val m_exp_label = new Label
  protected val m_or_label  = new Label
  protected val m_qualite_musique_label  = new Label
  protected val m_prix_augmenter_musique_label  = new Label
  protected val m_menu_user_info : BoxPanel = new BoxPanel(Orientation.Vertical) {
    contents += new Label{
      text = "Joueur :"
      font = new Font("Ariel", java.awt.Font.ITALIC | java.awt.Font.BOLD,16)
    }
    contents += Swing.VStrut(10)
    contents += m_niv_label
    contents += m_vie_label
    contents += m_exp_label
    contents += m_or_label
    contents += m_qualite_musique_label
    contents += m_prix_augmenter_musique_label
    contents += new Button
    {
      action = Action("Améliorer musique"){
        dj.augmenter_qualite_musique
        Data.this.update_all
      }
    }
  }
  def get_menu_user_info = {m_menu_user_info}
  def update_menu_user_info {
    m_niv_label.text = "Niveau : " + president.get_current_niveau
    m_vie_label.text = "Vie restante : " + dj.get_vie
    m_exp_label.text = "Experience : " + dj.get_experience
    m_or_label.text = "Or : " + dj.get_or
    m_qualite_musique_label.text = "Qualité musique : " + dj.get_qualite_musique
    m_prix_augmenter_musique_label.text = "Prix augmenter musique : " + dj.get_prix_amelioration_musique
    // Update
    m_menu_user_info.repaint()
    m_menu_user_info.revalidate()
  }

  // Update the menu that displays the currently selected case
  protected var m_selected_case_panel_tmp : BoxPanel = new BoxPanel(Orientation.Vertical)
  protected val m_selected_case_panel : BoxPanel = new BoxPanel(Orientation.Vertical)
  {
    contents += new Label{
      text = "Case sélectionnée :"
      font = new Font("Ariel", java.awt.Font.ITALIC | java.awt.Font.BOLD,16)
    }
    contents += Swing.VStrut(10)
    contents += m_selected_case_panel_tmp}

  def get_selected_case_panel = {m_selected_case_panel}
  def update_selected_case_panel {
    m_selected_case_panel.contents -= m_selected_case_panel_tmp
    m_case_actuellement_selectionnee match {
      case None =>
        m_selected_case_panel_tmp = new BoxPanel(Orientation.Vertical)
        {
          contents += new Label{text = "Pas de case sélectionnée"}
        }
      case Some((i,j)) =>
        val m_bde_opt = kokarde.membre_bde_present(i,j)
        val m_bde_usine_opt = m_bde_opt match {case None => None case Some(m) => Some (m.get_usine)}
        m_selected_case_panel_tmp = this.get_box_panel_of_membre_bde(m_bde_usine_opt,m_bde_opt)
        m_selected_case_panel.contents += m_selected_case_panel_tmp
    }
    // Update
    m_selected_case_panel.repaint()
    m_selected_case_panel.revalidate()
  }
  def get_box_panel_of_membre_bde(membre_bde_opt : Option[Membre_bde_usine],peut_faire_actions:Option[Membre_bde]) =
  {
    membre_bde_opt match {
      case None =>
        new BoxPanel(Orientation.Vertical)
        {
          contents += new Label{text = "Pas de membre bde sélectionné"}
        }
      case Some(m) =>
        new BoxPanel(Orientation.Vertical)
        {
          contents += new Label{text = "Nom : " + m.get_pretty_nom}
          contents += new Label {
            val s = new Dimension(60, 60)
            minimumSize = s
            maximumSize = s
            preferredSize = s
            icon = svg_images.resized_icon_of_file(m.get_nom ++ ".png",60,60)
          }
          contents += new Label{text = "Prix d'achat : " + m.get_prix}
          contents += new Label{text = "Prix d'amélioration : " + m.get_prix_amelioration}
          contents += new Label{text = "Prix de revente : " + m.get_prix_revente}
          // TODO : afficher plein d'informations cools sur le projectile...
          peut_faire_actions match {
            case None => ()
            case Some(m_bde) =>
              contents += new Button
              {
                action = Action("Revendre"){
                  kokarde.detruire_membre_bde(m_bde)
                  Data.this.update_all
                }
              }
              m_bde.get_amelioration_bde match {
                case None => ()
                case Some(_) =>
                contents += new Button
                {
                  action = Action("Améliorer") {
                    kokarde.ameliorer_membre_bde(m_bde)
                    Data.this.update_all
                  }
                }
              }
          }
        }
    }
  }

  // Update the menu that displays the currently selected member of the bde
  protected var m_menu_selected_membre_bde_tmp : BoxPanel = new BoxPanel(Orientation.Vertical)
  protected val m_menu_selected_membre_bde : BoxPanel = new BoxPanel(Orientation.Vertical)
  {
    contents += new Label{
      text = "Membre bde sélectionné :"
      font = new Font("Ariel", java.awt.Font.ITALIC | java.awt.Font.BOLD,16)
    }
    contents += Swing.VStrut(10)
    contents += m_menu_selected_membre_bde_tmp}
  def get_menu_selected_membre_bde = {m_menu_selected_membre_bde}
  def update_menu_selected_membre_bde {
    m_menu_selected_membre_bde.contents -= m_menu_selected_membre_bde_tmp
    m_menu_selected_membre_bde_tmp = this.get_box_panel_of_membre_bde(m_selected_membre_bde,None)
    m_menu_selected_membre_bde.contents += m_menu_selected_membre_bde_tmp
    // Update
    m_menu_selected_membre_bde.repaint()
    m_menu_selected_membre_bde.revalidate()
  }
  // Update all graphics
  def update_all {
    this.update_canvas
    this.update_menu_user_info
    this.update_menu_selected_membre_bde
    this.update_selected_case_panel
    this.update_menu_choose_membres_bde
  }
  // Here are some shortcuts
  def get_nb_lines = {kokarde.get_nbr_lignes}
  def get_nb_cols  = {kokarde.get_nbr_colonnes}

  // Here is the "main loop"
  def main_loop() {
    def main_loop_aux() {
      val (etat, message) = president.etape_suivante
      this.update_all
      if(message != "") {this.send_message(message)}
      etat match {
        case Mini_steps(_) => ()
        case _ => timer_non_recursif.stop()
      }
    }
    timer_non_recursif.set_action(main_loop_aux)
    timer_non_recursif.start()
  }
}

// ----------------------------------------
// ------- Menu bouton membres bde --------
// ----------------------------------------
// Ce est très utile pour afficher des
// informations sur le membre bde actuellement
// séléctionné
class Menu_button_membre_bde(main_data : Data, associated_membre_bde : Membre_bde_usine, active:Boolean) extends Button {
  val button_size = 60
  val m_associated_membre_bde = associated_membre_bde
  val m_active = active
  // ########## Style ##########
  focusPainted=false
  contentAreaFilled=false
  opaque=false
  // ########## Actions ##########
  def when_clicked()
  {
    main_data.set_membre_bde_selectionne(m_associated_membre_bde)
    main_data.send_message("Le membre bde " ++ m_associated_membre_bde.get_pretty_nom ++ " a bien été sélectionné.")
    main_data.update_all
  }
  // Ne pas inverser action et icon...
  action = Action("") {when_clicked()}
  // ########## Icon ##########
  val my_image = svg_images.resized_image_of_file(m_associated_membre_bde.get_nom ++ ".png",button_size,button_size)
  if (m_active)
    icon = svg_images.resized_icon_of_file(m_associated_membre_bde.get_nom ++ ".png",button_size,button_size)
  else
    icon = svg_images.resized_icon_grayscale_of_file(m_associated_membre_bde.get_nom ++ ".png",button_size,button_size)
  val s = new Dimension(button_size, button_size)
  minimumSize = s
  maximumSize = s
  preferredSize = s
}

// ----------------------------------------
// ----------- Menu bouton next -----------
// ----------------------------------------
// Ce bouton est utile pour afficher
// la fleche (=>) suivant en haut de la fenêtre
class Menu_button_next_step(main_data : Data) extends Button {
  val button_size = 60
  // ########## Style ##########
  focusPainted=false
  contentAreaFilled=false
  opaque=false
  // ########## Actions ##########
  def when_clicked()
  {
    main_data.get_current_state match
      {
        case Positionner_membres_bde() =>
          main_data.send_message("Etape suivante...")
          timer_non_recursif.set_delay(timer_non_recursif.get_default_delay)
          main_data.clear_membre_bde_selectionne
          main_data.clear_case_actuellement_selectionnee
          main_data.main_loop
        case Mini_steps(_) =>
          timer_non_recursif.set_delay(math.max(1,(timer_non_recursif.get_delay/2)))
        case _ => ()
      }
  }
  // Ne pas inverser action et icon...
  action = Action("") {when_clicked()}
  // ########## Icone ##########
  icon = svg_images.resized_icon_of_file("next_step.png", button_size, button_size)
  val s = new Dimension(button_size, button_size)
  minimumSize = s
  maximumSize = s
  preferredSize = s
}

// ----------------------------------------
// ---------------- Canvas ----------------
// ----------------------------------------
// Le canvas est utilisé pour dessiner le
// plateau de jeux
class Canvas(main_data : Data) extends Panel {
  var n_lines = main_data.get_nb_lines
  var n_cols = main_data.get_nb_cols

  // Todo : update these info only when they change... (pb : detect change)
  def size_block () = Math.min(size.width/n_cols,size.height/n_lines)
  def offsets() = {
    val s = size_block()
    ((size.width - s * n_cols)/2,(size.height - s * n_lines)/2)
  }
  def xy_of_coord(coord:(Int,Int)) = { coord match { case(l,c) =>
    val s = size_block()
    val (x_off,y_off) = offsets()
    (x_off + c*s, y_off + l*s)
  }}
  def coord_of_xy(x : Int, y:Int) = {
    val (x_off,y_off) = offsets()
    ((y-y_off)/size_block(), (x-x_off)/size_block())
  }
  def display_file_at_coord(g:Graphics2D,coord:(Int,Int), file:String, size:Int)
  {
    val img = svg_images.resized_image_of_file(file,size,size)
    val (x1,y1) = xy_of_coord(coord)
    g.drawImage(img,x1,y1,null)
  }
  def xy_of_movable(m : Movable) = {
    val (x1,y1) = xy_of_coord(m.get_coord)
    val (x2,y2) = xy_of_coord(m.get_next_coord)
    val x3 = x1 + ((x2-x1)*m.get_current_mini_steps)/m.get_nb_mini_steps
    val y3 = y1 + ((y2-y1)*m.get_current_mini_steps)/m.get_nb_mini_steps
    (x3,y3)
  }
  def display_movable(g:Graphics2D, name : String, m : Movable, size:Int)
  {
    val img = svg_images.resized_image_of_file(name + ".png",size,size)
    val (x,y) = xy_of_movable(m)
    g.drawImage(img,x,y,null)
  }
  def display_etudiant(g:Graphics2D, name : String, e : Etudiant, s:Int)
  {
    display_movable(g,name,e,s)
    // Display an other picture depending on the state
    if(e.dans_coma)
      display_movable(g,"etat_coma",e,s)
    if(e.danse)
      display_movable(g,"etat_danse",e,s)
    if(e.fume)
      display_movable(g,"etat_fumer",e,s)
    if(e.get_but == But_DJ())
      display_movable(g,"etat_dj",e,s)
    if(e.est_avec_bouclette)
      display_movable(g,"etat_bouclette",e,s)
    // Display a life bar
    val (x,y) = xy_of_movable(e)
    g.setColor(Color.black)
    g.drawRect(x,y,s,s/10)
    g.setColor(Color.white)
    g.fillRect(x,y,s,s/10)
    g.setColor(Color.red)
    g.fillRect(x,y,(s*e.get_boure.toInt)/100,s/10)
  }
  def display_projectile(g:Graphics2D, p : Projectile, size:Int)
  {
    val img = svg_images.resized_image_of_file(p.get_nom + ".png",size,size)
    val (x1,y1) = xy_of_coord(p.get_coord)
    val (x2,y2) = xy_of_movable(p.get_cible)
    val x3 = x1 + ((x2-x1)*p.get_current_mini_steps)/p.get_nb_mini_steps
    val y3 = y1 + ((y2-y1)*p.get_current_mini_steps)/p.get_nb_mini_steps
    g.drawImage(img,x3,y3,null)
  }
  override def paintComponent(g: Graphics2D) {
    super.paintComponent(g)
    g.setRenderingHint(java.awt.RenderingHints.KEY_INTERPOLATION,
      java.awt.RenderingHints.VALUE_INTERPOLATION_BICUBIC)
    g.setRenderingHint(java.awt.RenderingHints.KEY_RENDERING,
      java.awt.RenderingHints.VALUE_RENDER_QUALITY);
    g.setRenderingHint(java.awt.RenderingHints.KEY_ANTIALIASING,
      java.awt.RenderingHints.VALUE_ANTIALIAS_ON)
    main_data.get_current_state match {
      case Perdu() =>
        g.setColor(Color.black)
        g.fillRect(0,0,size.width,size.height)
        g.setColor(Color.red)
        g.setFont(new Font("TimesRoman", java.awt.Font.PLAIN, 30));
        val s = "Perdu !"
        val s_w = g.getFontMetrics().stringWidth(s);
        g.drawString(s, size.width/2-s_w/2, size.height/2)
        g.setFont(new Font("TimesRoman", java.awt.Font.PLAIN, 15));
        val s2 = "Clique pour recommencer"
        val s2_w = g.getFontMetrics().stringWidth(s2);
        g.drawString(s2, size.width/2-s2_w/2, size.height/2+60)
      case Gagne() =>
        g.setColor(Color.black)
        g.fillRect(0,0,size.width,size.height)
        g.setColor(Color.red)
        g.setFont(new Font("TimesRoman", java.awt.Font.PLAIN, 30));
        val s = "Gagné !"
        val s_w = g.getFontMetrics().stringWidth(s);
        g.drawString(s, size.width/2-s_w/2, size.height/2)
        g.setFont(new Font("TimesRoman", java.awt.Font.PLAIN, 15));
        val s2 = "Clique pour recommencer"
        val s2_w = g.getFontMetrics().stringWidth(s2);
        g.drawString(s2, size.width/2-s2_w/2, size.height/2+60)
      case _ =>
        val s = size_block()
        // Draw random background here
        g.setColor(Color.black)
        val (x1,y1) = xy_of_coord((0,0))
        g.fillRect(x1,y1,s*n_cols,s*n_lines)
        // Draw the cases by type
        val cases = kokarde.get_cases
        cases.foreach{case (type_case,_) =>
          val file = kokarde.file_of_case(type_case) ++ ".png";
          cases(type_case).foreach{case (coord,_) => display_file_at_coord(g,coord,file,s)}
        }
        // Draw membres_bde
        kokarde.get_membres_bde.map(m => display_movable(g,m.get_nom,m,s))
        // Draw students
        kokarde.get_etudiants.map(e => display_etudiant(g,e.get_nom,e,s))
        // Draw projectiles
        kokarde.get_projectiles.map(p => display_projectile(g,p,s))
        // Draw lines
        // g.setColor(Color.black)
        // for(i <- 0 to n_lines) {
        //   val (x1,y1) = xy_of_coord(i,0)
        //   val (x2,y2) = xy_of_coord(i,n_cols)
        //   g.drawLine(x1,y1,x2,y2)
        // }
        // Draw cols
        // for(j <- 0 to n_cols) {
        //   val (x1,y1) = xy_of_coord(0,j)
        //   val (x2,y2) = xy_of_coord(n_lines,j)
        //   g.drawLine(x1,y1,x2,y2)
        // }
        // DEPRACATED : since only one student can be in a given position,
        // this section isn't usefull anymore.
        // Draw the little number above the membres_bde. The structure
        // used isn't the best one, but it's not sensitive here
        // g.setFont(new Font("TimesRoman", java.awt.Font.PLAIN, 12));
        // for (i <- 0 until n_lines)
        // {
        //   for (j <- 0 until n_cols)
        //   {
        //     val n = main_data.get_###_plateau_###.compter_monstres_en_position((i,j))
        //     if (n>0)
        //     {
        //       val (x,y) = xy_of_coord(i,j)
        //       g.setColor(Color.green)
        //       g.fillRect(x+1,y+1,12,12)
        //       g.setColor(Color.black)
        //       g.drawString(n.toString, x+4, y+11)
        //     }
        //   }
        // }
        // Dessiner un cadre bleu autour de la case sélectionnée
        main_data.get_case_actuellement_selectionnee match {
          case None => ()
          case Some((i,j)) =>
            g.setColor(Color.blue)
            val (x1,y1) = xy_of_coord(i,j)
            g.drawRect(x1,y1,s,s)
        }
    }
  }
}

// ----------------------------------------
// --------------- Fenêtre ----------------
// ----------------------------------------
// La fenêtre appelle les différents éléments graphiques
// définis plus haut
object GUIKokardeDefense extends SimpleSwingApplication {
  def top = new MainFrame { // top is a required method
    title = "Kokarde-Defense"
    minimumSize = new Dimension(800,600)
    maximize()
    // declare Components here
    val titre = new Label {
      text = "Kokarde-Defense"
      font = new Font("Ariel", java.awt.Font.ITALIC, 24)
      opaque=true
      background = Color.green
    }
    val selection_membres_bde = new Label("Membres BDE disponibles :")
    val message_box = new TextArea {
      // columns = 10
      text = "Bienvenue dans notre Kokarde-Defense ! A vous de protéger le DJ des étudiants mécontants..."
      editable = false
      border = Swing.EmptyBorder(10, 10, 10, 10)
    }
    def send_message(message : String) {
      message_box.text = message
    }
    val main_data = new Data(send_message)
    val canvas = new Canvas(main_data) {
      preferredSize = new Dimension(100, 100)
    }
    main_data.define_update_canvas(canvas.repaint)
    // ########## Architecture principale ##########
    val top_menu = new GridPanel(2, 1) {
      contents += titre
      contents += main_data.get_menu_choose_membres_bde
    }
    val left_menu = new BoxPanel(Orientation.Vertical) {
      contents += main_data.get_menu_user_info
      contents += Swing.VStrut(20)
      contents += Swing.Glue
      contents += main_data.get_selected_case_panel
      contents += Swing.Glue
      border = Swing.EmptyBorder(10, 10, 10, 10)
      preferredSize = new Dimension(250,250)
    }
    val right_menu = new BoxPanel(Orientation.Vertical) {
      contents += Swing.Glue
      contents += main_data.get_menu_selected_membre_bde
      contents += Swing.Glue
      border = Swing.EmptyBorder(10, 10, 10, 10)
      preferredSize = new Dimension(300,300)
    }
    // ----- Positionnement des panneaux -----
    contents = new BorderPanel {
      layout(top_menu) = North
      layout(left_menu) = West
      layout(canvas) = Center
      layout(right_menu) = East
      layout(message_box) = South
    }
    size = new Dimension(300, 200)
    // On ajoute un menu
    menuBar = new MenuBar {
      contents += new Menu("Fichier") {
        contents += new MenuItem(Action("Nouvelle partie") {
          president.recommencer_partie
          timer_non_recursif.set_delay(timer_non_recursif.get_default_delay)
          main_data.clear_membre_bde_selectionne
          main_data.clear_case_actuellement_selectionnee
          main_data.update_all
          president.set_etat(Perdu())
        })
        contents += new MenuItem(Action("Quitter") {
          sys.exit(0)
        })
      }
    }

    // specify which Components produce events of interest
    listenTo(canvas.mouse.clicks)
    main_data.update_all
    // react to events
    reactions += {
      case MouseClicked(_, point, _, _, _) =>
        main_data.get_current_state match {
          case Perdu() =>
            president.recommencer_partie
          case Gagne() =>
            president.recommencer_partie
          case _ =>
            val (line,col) = canvas.coord_of_xy(point.x,point.y)
            if (line >= 0 && line < main_data.get_nb_lines && col >= 0 && col < main_data.get_nb_cols)
            {
              main_data.set_case_actuellement_selectionnee(line,col)
              kokarde.etudiant_present((line,col)) match {
                case Some(tower) =>
                  main_data.send_message("Un membre BDE a été trouvé sur la case actuelle !")
                case None =>
                  main_data.get_membre_bde_selectionne match {
                    case None =>
                      main_data.send_message("Veuillez sélectionner un membre BDE à placer.")
                    case Some(membre_bde_usine) =>
                      main_data.send_message("Le membre BDE " ++ membre_bde_usine.get_pretty_nom ++ " va être placé.")
                      try {
                        kokarde.creer_membre_bde(membre_bde_usine,(line,col))
                      } catch {
                        case e: IllegalStateException =>
                          main_data.clear_membre_bde_selectionne
                          main_data.send_message("Erreur : " + e.getMessage)
                      }
                  }
              }
            }
        }
        main_data.update_all
    }
  }
}
