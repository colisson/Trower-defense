package kokarde_defense

import kokarde._

import scala.collection.mutable._
import math._

class Path_astar(type_etud:Type_etudiant,etudiant:(Int,Int),objectif:(Int,Int))
{
  protected var m_type_etud = type_etud
  protected var m_path = map_path.get_path(type_etud,But_quitte_kokarde())
// La liste ouverte contient les cases, et leur parent (-1,-1) au début)
  protected var liste_ouverte:Map[(Int,Int),(Int,Int)] = Map()
  protected var liste_fermee:Map[(Int,Int),(Int,Int)] = Map((objectif -> objectif))
  protected var case_courante = objectif

  protected def distance_oiseau(c:(Int,Int)) = math.pow(etudiant._1 - c._1,2) + math.pow(etudiant._2 - c._2,2)


  protected def est_accessible(c:(Int,Int)):Boolean = {
    (kokarde.est_accessible_absolu(m_type_etud,c,false))
  }
  protected def est_traitee(c:(Int,Int)):Boolean = {liste_fermee contains c}
  protected def nest_pas_construite(c:(Int,Int)) = {
    !kokarde.contient_membre_bde(c)
  }
  protected def est_a_traiter(c:((Int,Int),(Int,Int))):Boolean = {
    c match {
      case (c,a) => (est_accessible(c) && !(est_traitee(c)) && (nest_pas_construite(c)|| (kokarde.membre_bde_present(c) match {case Some(membre) => membre.get_nom == "membre_bde_bouclette" case None => false})))
    }
  }
  protected def nettoyage_cases_a_traiter(cases:Map[(Int,Int),(Int,Int)]):(Map[(Int,Int),(Int,Int)]) = {
    if (cases.isEmpty)
      cases
    else
      cases.filter(est_a_traiter)
  }


protected def algo_astar {
  while (! liste_fermee.contains(etudiant)) {

    var i = case_courante._1
    var j = case_courante._2

    if (!liste_ouverte.contains((i+1,j))) liste_ouverte += ((i+1,j) -> case_courante)
    if (!liste_ouverte.contains((i-1,j))) liste_ouverte += ((i-1,j) -> case_courante)
    if (!liste_ouverte.contains((i,j-1))) liste_ouverte += ((i,j-1) -> case_courante)
    if (!liste_ouverte.contains((i,j+1))) liste_ouverte += ((i,j+1) -> case_courante)
    liste_ouverte = nettoyage_cases_a_traiter(liste_ouverte)

    var case_min = liste_ouverte.head._1
    var dist_min = distance_oiseau (case_min)

    liste_ouverte.keys.foreach{
      k => if (distance_oiseau(k) < dist_min)
      {case_min = k ; dist_min = distance_oiseau(case_min)}
    }
    liste_fermee += (case_min -> liste_ouverte(case_min))
    case_courante = case_min
    liste_ouverte -= (case_min)
  }
}

algo_astar


  def case_suivante (c:(Int,Int)):(Int,Int) = {
    liste_fermee(c)
} 








}
