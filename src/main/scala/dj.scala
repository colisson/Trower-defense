package kokarde_defense

// ========================================
// ===============   DJ  ==================
// ========================================
// Le joueur est à la place du DJ
// C'est ici qu'il pourra accumuler de l'argent
// et c'est aussi ici que l'on retrouvera la vie
// qu'il reste au DJ

object dj {
  protected var m_vie_dj      : Int = 100
  protected var m_or          : Int = 0
  protected var m_experience  : Int = 0
  // Plus la musique est bonne, plus les étudiants
  // passeront de temps à danser... Il est donc
  // nécessaire d'améliorer sa musique pour que
  // les étuandiants ne viennent pas renverser
  // de la bière sur le DJ !
  protected var m_qualite_musique : Int = 0 // Entre 0 et 100
  // -------- Gérer le jeux --------
  def reinitialiser {
    reinitialiser_dj
    reinitialiser_or
  }
  // -------- Gérer le DJ --------
  def reinitialiser_dj {m_vie_dj = 100}
  def get_vie = {m_vie_dj}
  def est_vivant = {m_vie_dj > 0}
  def set_vie(vie : Int) {m_vie_dj = vie}
  def perdre_vie(vie : Int) {m_vie_dj -= vie}
  // -------- Gérer l'or --------
  def reinitialiser_or {m_or = 0}
  def get_or = {m_or}
  def set_or(or : Int) {m_or = or}
  def gagne_or(or : Int) {m_or += or}
  def a_assez_d_or(or : Int) = {m_or >= or}
  def depense_or(or : Int) {
    if(a_assez_d_or(or))
      m_or -= or
    else
      throw new IllegalStateException ("Tu es trop pauvre")
  }
  // -------- Gérer l'expérience --------
  def get_experience = {m_experience}
  def set_experience(xp : Int) {m_experience += xp}
  // -------- Gérer la qualité de la musique --------
  // Plus la musique est bien, plus les joueurs dansent longtemps et souvent !
  def get_qualite_musique = {m_qualite_musique}
  def set_qualite_musique(n:Int) {m_qualite_musique = math.min(100,n)}
  def get_prix_amelioration_musique = {(100000*(10+get_qualite_musique))/110}
  def augmenter_qualite_musique = {
    depense_or(get_prix_amelioration_musique)
    set_qualite_musique(get_qualite_musique + 10)
  }
}
