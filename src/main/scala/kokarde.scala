package kokarde_defense

// ========================================
// =============  Kokarde  ================
// ========================================
// C'est le plateau de jeu.

import scala.collection.mutable._
import scala.io.Source
import java.util.Random

// Décrit le type sémantique de la case.
// Pour l'affichage, penser à modifier la fonction file_of_case pour
// dessiner la bonne image
abstract class Type_case
case class Case_depart            extends Type_case
case class Case_dehors            extends Type_case
case class Case_quitte_kokarde    extends Type_case
case class Case_mur               extends Type_case
case class Case_vomi_incruste     extends Type_case
case class Case_petit_vomi        extends Type_case
case class Case_bar               extends Type_case
case class Case_dj_perso          extends Type_case
case class Case_dj_platine        extends Type_case
case class Case_sol               extends Type_case
case class Case_porte_tna         extends Type_case
case class Case_sol_tna           extends Type_case

object kokarde {
  protected var m_nombre_lignes   : Int = -1
  protected var m_nombre_colonnes : Int = -1
  // Les cases ne sont pas codées dans un tableau, mais dans plusieurs
  // sous tableaux ayant des types différents. Le deuxième Int sert
  // lorsque la case peut-être amenée à disparaitre :
  // 0     : comme si n'était pas dans la case
  // n > 0 : est amené à disparaitre dans n tours
  // < 0  : ne disparait jamais
  protected var m_cases : Map[Type_case,Map[(Int,Int),Int]] = Map()
  protected var m_projectiles     : List[Projectile]        = List()
  protected var m_membres_bde     : List[Membre_bde]        = List()
  protected var m_etudiants       : List[Etudiant]          = List()
  protected var m_etudiants_coma  : Map[Etudiant,Boolean]   = Map ()
  // A chaque tour on mets à jour la carte des cases accessibles :
  // Ceci est utile pour avoir accès à l'information "y-a-t'il quelqu'un ici"
  // en O(1) modulo la mise à jour
  // /!\ Il faut bien penser à les mettre à jour !!!
  // Attention, pas plus d'un membre bde et d'un étudiant par case !!!
  protected var m_map_membres_bde : Map[(Int,Int),Membre_bde] = Map()
  protected var m_map_etudiants   : Map[(Int,Int),Etudiant]   = Map()
  
  protected var m_duree_petit_vomi : Int = 3
  // -------- Reinitialiser --------
  def reinitialiser {
    m_projectiles     = List()
    m_membres_bde     = List()
    m_etudiants       = List()
    m_map_membres_bde = Map()
    m_map_etudiants   = Map()
  }
  def get_rand = {m_rand}
  // -------- Gerer les cases --------
  protected var m_rand = new Random(System.currentTimeMillis())
  def file_of_case(type_case:Type_case) = {
    type_case match {
      case Case_depart()         => "case_depart"
      case Case_dehors()         => "case_dehors"
      case Case_quitte_kokarde() => "case_quitte_kokarde"
      case Case_mur()            => "case_mur"
      case Case_vomi_incruste()  => "case_vomi_incruste"
      case Case_petit_vomi()     => "case_petit_vomi"
      case Case_bar()            => "case_bar"
      case Case_dj_perso()       => "case_dj_perso"
      case Case_dj_platine()     => "case_dj_platine"
      case Case_sol()            => "case_sol"
      case Case_porte_tna()      => "case_porte_tna"
      case Case_sol_tna()        => "case_sol_tna"
    }
  }
  def get_nbr_lignes = {m_nombre_lignes}
  def set_nbr_lignes(l:Int) = {m_nombre_lignes = l}
  def get_nbr_colonnes = {m_nombre_colonnes}
  def set_nbr_colonnes(c:Int) = {m_nombre_colonnes = c}
  def get_duree_petit_vomi = {m_duree_petit_vomi}

  def set_case(coord:(Int,Int),c:Type_case,v:Int) = {
    m_cases get c match {
      case None => m_cases += (c -> Map(coord -> v))
      case Some(map) => map += (coord -> v)
    }
  }
  def case_dans_kokarde(c:(Int,Int)) = { c match { case (i,j) =>
    i >= 0 && i < get_nbr_lignes && j >= 0 && j < get_nbr_colonnes
  }}
  charger_kokarde_a_partir_dun_fichier_texte
  def charger_kokarde_a_partir_dun_fichier_texte {
    // # est un mur
    // D est le DJ (personnage)
    // d est le DJ (platine)
    // un . est le sol
    // un v est du petit vomi, un V du vomi incrusté
    // un B est le bar
    // un ° est dehors
    // un < est une case pour quitter la Kokarde
    // un S est un case départ
    // un P est une porte TNA
    // un T est le sol du TNA
    val lines = Source.fromInputStream(getClass.getResourceAsStream("kokarde_map.txt")).getLines().toList
    set_nbr_lignes(lines.length)
    set_nbr_colonnes(0)
    lines.zipWithIndex.map{ case (line,i) =>
      line.zipWithIndex.map{ case (letter,j) =>
        set_nbr_colonnes(math.max(get_nbr_colonnes,j+1))
        letter match {
          case 'S' => set_case((i,j),Case_depart(),-1)
          case '°' => set_case((i,j),Case_dehors(),-1)
          case '<' => set_case((i,j),Case_quitte_kokarde(),-1)
          case '#' => set_case((i,j),Case_mur(),-1)
          case 'V' => set_case((i,j),Case_vomi_incruste(),-1)
          case 'v' => set_case((i,j),Case_petit_vomi(),get_duree_petit_vomi)
          case 'B' => set_case((i,j),Case_bar(),-1)
          case 'D' => set_case((i,j),Case_dj_perso(),-1)
          case 'd' => set_case((i,j),Case_dj_platine(),-1)
          case '.' => set_case((i,j),Case_sol(),-1)
          case 'P' => set_case((i,j),Case_porte_tna(),-1)
          case 'T' => set_case((i,j),Case_sol_tna(),-1)
          case _   => ()
        }
      }
    }
  }

  // --- Questions fondamentales ---
  def est_dans_etat(e:Type_case,coord:(Int,Int))
  = {(m_cases contains e) && (m_cases(e) contains coord) && (m_cases(e)(coord) != 0)}
  def est_depart(c:(Int,Int))        = est_dans_etat(Case_depart(),c)
  def est_mur(c:(Int,Int))           = est_dans_etat(Case_mur(),c)
  def est_vomi_incruste(c:(Int,Int)) = est_dans_etat(Case_vomi_incruste(),c)
  def est_petit_vomi(c:(Int,Int))    = est_dans_etat(Case_petit_vomi(),c)
  def est_vomi(c:(Int,Int))          = (est_vomi_incruste(c) || est_petit_vomi(c))
  def est_bar(c:(Int,Int))           = est_dans_etat(Case_bar(),c)
  def est_dj_perso(c:(Int,Int))      = est_dans_etat(Case_dj_perso(),c)
  def est_dj_platine(c:(Int,Int))    = est_dans_etat(Case_dj_platine(),c)
  def est_dj(c:(Int,Int))            = {est_dj_platine(c) || est_dj_perso(c)}
  def est_sol(c:(Int,Int))           = est_dans_etat(Case_sol(),c)
  def est_sol_tna(c:(Int,Int))       = est_dans_etat(Case_sol_tna(),c)
  def est_porte(c:(Int,Int))         = est_dans_etat(Case_porte_tna(),c)
  def est_dehors(c:(Int,Int))        = est_dans_etat(Case_dehors(),c)
  def contient_etudiant(c:(Int,Int)) = {m_etudiants contains c}
  def contient_membre_bde(c:(Int,Int)) = {m_map_membres_bde contains c}
  // --- Ici on rajoute quelques fonctions de plus haut niveau ---
  def est_constructible(c:(Int,Int)) =
  {
    (est_sol(c) || est_dehors(c)) && ! contient_etudiant(c) && ! contient_membre_bde(c)
  }
  // Une case peut-être accessible "dans l'absolu" lorsque
  // le but n'entre pas en compte pour savoir si la case est
  // accessible.
  def est_accessible_absolu(t:Type_etudiant,c:(Int,Int),compte_etudiant:Boolean) =
  {
    ((c._1 >= 0 && c._1 < get_nbr_lignes
      && c._2 >= 0 && c._2 < get_nbr_colonnes)
      && ((t == Type_fantome())
        || ((est_depart(c) || est_sol(c) || est_dehors(c) || est_sol_tna(c) || est_porte(c) || est_vomi(c))
          && (! contient_etudiant(c) || !compte_etudiant)
          && (!contient_membre_bde(c) || (membre_bde_present(c) match { case Some(membre) => membre.get_nom == "membre_bde_bouclette" case None => false}))
          && (!est_vomi(c) || t == Type_sale())
          && (!est_porte(c) || t == Type_cle_trackNart()))))
  }
  // Lorsque la case est le but on autorise le jouer à y aller
  // dans le "path", mais si on voit qu'elle n'est pas accessible
  // en absolu alors le joueur reste sur place et passe au but
  // suivant.
  def est_accessible_type(t:Type_etudiant,b:But,c:(Int,Int)) =
  {
    est_accessible_absolu(t,c,true) || (get_cases_but(b) contains c)
  }
  def est_accessible_etudiant(e:Etudiant,c:(Int,Int)) =
  {
    est_accessible_type(e.get_type,e.get_but,c)
  }
  // --- Pour obtenir les Map de certains endroits particuliers
  def get_cases = {m_cases}
  def get_cases_no_fail(e:Type_case) = {m_cases getOrElse (e,Map())}
  def get_cases_depart = get_cases_no_fail(Case_depart())
  def get_random_case_depart = {
    val departs = get_cases_depart
    val size = departs.size
    if (size == 0)
      (1,1)
    else
      departs.iterator.drop(m_rand.nextInt(departs.size)).next._1}
  def get_cases_bar = get_cases_no_fail(Case_bar())
  def get_cases_dj = get_cases_no_fail(Case_dj_platine()) ++ get_cases_no_fail(Case_dj_perso())
  def get_cases_dehors = get_cases_no_fail(Case_dehors())
  def get_cases_quitte_kokarde = get_cases_no_fail(Case_quitte_kokarde())
  def get_cases_but(but:But) = {but match {
    case But_bar() => get_cases_bar
    case But_DJ() => get_cases_dj
    case But_fumer() => get_cases_dehors
    case But_quitte_kokarde() => get_cases_quitte_kokarde
  }}
  // -------- Gerer les étudiants --------
  def get_etudiants = {m_etudiants}
  def etudiant_present(coord : (Int,Int)) : Option[Etudiant]= {
    m_map_etudiants.get(coord)
  }
  // /!\ Il faut bien penser à lancer maj_map_etudiants après !
  def ajouter_etudiant(e:Etudiant) {m_etudiants = e :: m_etudiants}
  // Cette fonction sert à mettre à jour les maps sur les etudiants
  def maj_map_etudiants {
    m_map_etudiants = Map()
    m_etudiants.foreach(e => m_map_etudiants += (e.get_pos_ij -> e))
  }
  def enlever_etudiants_morts {
    m_etudiants = m_etudiants.filter(! _.est_mort)
    maj_map_etudiants // Faculatif si on les fait bouger en même temps ?
  }
  def deplacer_etudiants {
    m_etudiants.foreach{_.avancer_mini_step}
  }
  // -------- Gerer la liste des comas (soins)----- 
  def add_etudiant_coma(fragile:Etudiant) = {m_etudiants_coma += (fragile -> false)}
  def del_etudiant_coma(survivor:Etudiant)= {m_etudiants_coma -= survivor}
  def n_est_pas_pris_en_charge(c:(Etudiant,Boolean)):Boolean = {!(c._2)}
  def get_etudiant_seul:Option[Etudiant] = {var aux = m_etudiants_coma.filter(n_est_pas_pris_en_charge)
    if (aux.isEmpty)
      {None}
      else {
        m_etudiants_coma(aux.head._1) = false
        Some(aux.head._1)
      }
  }

  // -------- Gerer les membres bde --------
  def membre_bde_present(coord : (Int,Int)) : Option[Membre_bde] = {
    m_map_membres_bde.get(coord)
  }
  def get_membres_bde = {m_membres_bde}
  def creer_membre_bde(u:Membre_bde_usine,c:(Int,Int)) {
    val m = u(c)
    if(! est_constructible(c))
      throw new IllegalStateException("Case non constructible")
    else if (! dj.a_assez_d_or(m.get_prix))
      throw new IllegalStateException("Vous n'avez pas assez d'argent")
    else
    {
      m_map_membres_bde += (m.get_coord -> m)
      m_membres_bde = m :: m_membres_bde
      // On vérifie que tous les buts sont toujours accessibles
      if (map_path.maj)
      {
        // map_path.display_paths
        dj.depense_or(m.get_prix)
      }
      else
      { // On annule la contruction de la tour
        m_map_membres_bde -= m.get_coord
        m_membres_bde = m_membres_bde.tail
        throw new IllegalStateException("Vous ne pouvez pas barrer la route aux étudiants !")
      }

    }
  }
  def detruire_membre_bde_position(c:(Int,Int)) {
    m_map_membres_bde -= c
    m_membres_bde = m_membres_bde.filter(m =>
      if (m.get_coord != c)
        true
      else
      {
        dj.gagne_or(m.get_prix_revente)
        false}
    )
  }
  def detruire_membre_bde(membre_bde : Membre_bde) {
    dj.gagne_or(membre_bde.get_prix_revente)
    m_map_membres_bde -= membre_bde.get_coord
    m_membres_bde = m_membres_bde.filter(m => m != membre_bde)
  }
  def ameliorer_membre_bde(membre_bde : Membre_bde) {
    membre_bde.get_amelioration_bde match {
      case None => throw new IllegalStateException("Ce membre BDE ne peut pas être amélioré")
      case Some(m_u) =>
      if (! dj.a_assez_d_or(membre_bde.get_prix_amelioration))
        throw new IllegalStateException("Vous n'avez pas assez d'argent")
      else
      {
        val m = m_u(membre_bde.get_coord)
        m_map_membres_bde -= membre_bde.get_coord
        m_membres_bde = m_membres_bde.filter(m => m != membre_bde)
        m_map_membres_bde += (m.get_coord -> m)
        m_membres_bde = m :: m_membres_bde
        dj.depense_or(membre_bde.get_prix_amelioration)
      }

    }
  }
  // -------- Gerer les tirs --------
  def get_projectiles = {m_projectiles}
  def ajouter_projectile(p:Projectile) {m_projectiles = p :: m_projectiles}
  def enlever_projectiles_morts {
    m_projectiles = m_projectiles.filter{p => !p.a_explose}
  }
  def tirs_des_membres_bde {
    m_membres_bde.foreach{_.tirer}
    enlever_etudiants_morts
    enlever_projectiles_morts
  }
  def bouger_projectiles {m_projectiles.foreach{_.avancer_mini_step}}
}
