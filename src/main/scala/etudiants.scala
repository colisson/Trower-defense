package kokarde_defense

// ========================================
// ============  Etudiants  ===============
// ========================================
// Les étudiants sont l'équivalent des "monstres"
// dans les tower defense classiques

import kokarde._
// Les Map sont une bonne alternative entre les listes
// (que l'on peut mapper) et les tableaux (accessibilité
// en temps constant)
import scala.collection.mutable._

// Chaque étudiant a un but.
// L'idée est que chaque étudiant doit d'abord aller au bar,
// prendre une bière, puis aller la jeter sur le DJ. Une fois la bière jetée
// ils peuvent ensuite aller fumer, ou retourner la jeter sur le DJ.
// Ces étapes intermédiaires nécésitent donc l'introduction de buts.
// Penser à bien mettre à jour l'objet qui suit la déclaration pour
// avoir la bonne liste
abstract class But
case class But_bar            extends But
case class But_DJ             extends But
case class But_fumer          extends But
case class But_quitte_kokarde extends But
case class But_soin(a_sauver:Option[Etudiant]) extends But
object Liste_buts {
  protected val l = List(But_bar(),But_DJ(),But_fumer(),But_quitte_kokarde())
  def get_l = l
}

// Un étudiant peut de plus être dans plusieurs états :
// - Actif   : c'est l'état par défaut, l'étudiant va essayer d'effectuer
//               le but qui lui est attribué
// - Danse   : L'étudiant fait une pause et danse.
// - Bouclette : L'étudiant... "danse sur place"
// - Coma    : L'étudiant est dans le coma. Si aucun étudiant sauveteur
//              n'est venu le soigner au bout de n tours (au début n=3)
//              il meurt. A noter que quand un étudiant est dans le coma
//              il est toujours sur le plateau et empèche les autres
//              étudiants de passer
// - Mort    : L'étudiant est mort (ou à l'hôpital). Bref il n'embêtera
//              plus le DJ
abstract class Etat_etudiant
case class Etat_actif            extends Etat_etudiant
case class Etat_danse(n:Int)     extends Etat_etudiant
case class Etat_fume(n:Int)      extends Etat_etudiant
case class Etat_bouclette(n:Int) extends Etat_etudiant
case class Etat_coma(n:Int)      extends Etat_etudiant
case class Etat_mort             extends Etat_etudiant
case class Etat_quitte_kokarde   extends Etat_etudiant

// Chaque étudiant a un type particulier, certains
// ont par exemple les clés pour passer par
// des raccourcis, d'autres peuvent passer sur le vomi
// sans que ça les dérange, d'autres peuvent soigner
// les autres étudiants...
abstract class Type_etudiant
case class Type_basique       extends Type_etudiant // euh...
case class Type_sale          extends Type_etudiant // peut passer par le vomi
case class Type_soigneur      extends Type_etudiant // rescucite les élèves en coma
case class Type_cle_trackNart extends Type_etudiant // peut passer au tna
case class Type_fantome       extends Type_etudiant // Sushi nous hante !!!
case class Type_sage          extends Type_etudiant // ne boit pas d'alcool (implémenter les non tirs de tour)
object Liste_types_etudiant {
  protected val l = List(Type_basique(),Type_sale(),Type_soigneur(),Type_cle_trackNart(),Type_fantome(),Type_sage())
  def get_l = l
}


// L'usine a étudiants
abstract class Etudiant_usine extends Nameable {
  def apply(coord:(Int,Int)) : Etudiant
  // Informations générales sur l'étudiant
  // Plus de nombre est grand, plus il est lent
  var m_default_nb_steps:Int
  // A chaque verre bu, l'étudiant est un peu plus bourré. Sa capacité
  // a être bourré dépend de deux choses : sa résistance naturelle,
  // ainsi que le verre qui lui est servi (produit des deux)
  protected var m_boure_par_verre : Double
  // Quand on est dans le coma on y reste pendant n tours avant de mourir :
  protected var m_max_coma : Int
  // A chaque tour les étudiants reprennent un peu de leurs esprits
  // en diminuant leur alcoolemie
  protected var m_reprendre_esprit : Double
  // A chaque passage au bar, l'étudiant donne de l'argent au BDE
  // pour payer la bière qu'il emprunte... Enfin sauf exception :
  // certains volent des bières... voir piquent dans la caisse !
  protected var m_or_passage_bar : Int
  // A chaque passage au bar, l'étudiant peut boire un peu plus...
  protected var m_alcoolemie_passage_bar : Double
  // Quantité de bierre à verser sur le DJ qu'il peut
  // transporter à chaque voyage
  protected var m_qte_bierre_transportable : Int
  // Le type de l'étudiant
  protected var m_type : Type_etudiant
  // Permet de gagner de l'argent (ou en perdre) quand l'étudiant meurt
  protected var m_gain_or_quand_mort : Int
  protected var m_proba_aller_fumer : Int // Entre 0 et 100
  // -------- Getter --------
  def get_default_nb_steps = {m_default_nb_steps}
  def get_boure_par_verre = {m_boure_par_verre}
  def get_max_coma = {m_max_coma}
  def get_reprendre_esprit = {m_reprendre_esprit}
  def get_or_passage_bar = {m_or_passage_bar}
  def get_alcoolemie_passage_bar = {m_alcoolemie_passage_bar}
  def get_qte_bierre_transportable = {m_qte_bierre_transportable}
  def get_type = {m_type}
  def get_gain_or_quand_mort = {m_gain_or_quand_mort}
  def get_proba_aller_fumer = {m_proba_aller_fumer}
  def get_proba_aller_danser : Int // Cette fonction peut dépendre de la qualité de la musique...
  def get_temps_danse : Int        // Cette fonction peut dépendre de la qualité de la musique...
  // -------- Fonctions pratiques --------
  def peut_marcher_sur_vomi = {get_type match {case Type_sale() => true; case _ => false}}
}
// Et la classe étudiant en elle-même. Elle a besoin
// d'être surchargée seulement pour la fonction
// alea_en_fct_de_bouree en théorie, mais si on veut
// faire des choses plus originales c'est toujours possible
// d'overrider d'autre fonctions
abstract class Etudiant(etudiant_usine:Etudiant_usine,coord:(Int,Int))
    extends Movable(etudiant_usine.get_default_nb_steps,coord,coord)
{
  protected val m_etudiant_usine : Etudiant_usine = etudiant_usine
  protected var m_etat : Etat_etudiant = Etat_actif()
  protected var m_but : But   = But_bar()
  // Lorsque l'on met une penalité Some(n,new_nb_steps) le joueur
  // va a la vitesse new_nb_steps pendant n grands pas
  protected var m_nb_steps_penalite : Option[(Int,Int)] = None
  // m_alcoolemie = 0 si tout va bien, m_alcoolemie = 100
  // quand il tombe dans le coma
  protected var m_alcoolemie : Double = 0.



  // -------- Getter des types fondamentaux --------
  def get_usine = {m_etudiant_usine}
  def get_nom = {get_usine.get_nom}
  def get_pretty_nom = {get_usine.get_pretty_nom}
  def get_default_nb_steps = {m_etudiant_usine.get_default_nb_steps}
  def get_boure_par_verre = {m_etudiant_usine.get_boure_par_verre}
  def get_max_coma = {m_etudiant_usine.get_max_coma}
  def get_reprendre_esprit = {m_etudiant_usine.get_reprendre_esprit}
  def get_or_passage_bar = {m_etudiant_usine.get_or_passage_bar}
  def get_alcoolemie_passage_bar = {m_etudiant_usine.get_alcoolemie_passage_bar}
  def get_qte_bierre_transportable = {m_etudiant_usine.get_qte_bierre_transportable}
  def get_type = {m_etudiant_usine.get_type}
  def get_gain_or_quand_mort = {m_etudiant_usine.get_gain_or_quand_mort}
  def get_proba_aller_fumer = {m_etudiant_usine.get_proba_aller_fumer}
  def get_proba_aller_danser = {m_etudiant_usine.get_proba_aller_danser}
  def get_temps_danse = {m_etudiant_usine.get_temps_danse}
  def peut_marcher_sur_vomi = {m_etudiant_usine.peut_marcher_sur_vomi}
  def filtrer_non_tirable = { 
    (! this.dans_coma) && (! this.est_mort) && (! this.quitte_kokarde)}
  def set_nb_steps_penalite(n:Int,new_nb_steps:Int) {
    m_nb_steps_penalite = Some(n,new_nb_steps)
  }
  def enlever_penalite {m_nb_steps_penalite = None}
  override def get_nb_mini_steps = {m_nb_steps_penalite match {
    case None => get_default_nb_steps
    case Some((_,nb_steps)) => nb_steps
  }}
  // -------- Gerer le but --------
  def get_but = {m_but}
  def set_but(b : But) {m_but = b}
  def set_but_soin {get_etudiant_seul match 
    {
    case None => m_but = But_soin(None)
    case Some(etud) => m_but = But_soin(Some(etud))
    set_chemin(get_type,get_coord,etud.get_coord)
    }
  }
  // -------- Gerer l'état --------
  def get_etat = {m_etat}
  def set_etat(e : Etat_etudiant) {
    m_etat = e
    e match
    {
      case Etat_quitte_kokarde() => set_but(But_quitte_kokarde())
      case Etat_coma(n)          => (if (n<=0) this.tuer)
      case Etat_danse(n)         => (if (n<=0) m_etat = Etat_actif())
      case Etat_fume(n)          => (if (n<=0) m_etat = Etat_actif())
      case Etat_bouclette(n)     => (if (n<=0) m_etat = Etat_actif())
      case _ => ()
    }
  }
  def renvoyer_kokarde {this.set_etat(Etat_quitte_kokarde())}
  def tuer {
    m_etat = Etat_mort()
    dj.gagne_or(get_gain_or_quand_mort)
    del_etudiant_coma(this)
  }
  def est_actif = {m_etat match {
    case Etat_actif() => true
    case _ => false}}
  def danse = {m_etat match {
    case Etat_danse(n) => true
    case _ => false}}
  def fume = {m_etat match {
    case Etat_fume(n) => true
    case _ => false}}
  def est_avec_bouclette = {m_etat match {
    case Etat_bouclette(n) => true
    case _ => false}}
  def dans_coma = {m_etat match {
    case Etat_coma(n) => n > 0
    case _ => false}}
  def est_mort = {m_etat match {
    case Etat_mort() => true
    case _ => false}}
  def quitte_kokarde = {m_etat match {
    case Etat_quitte_kokarde() => true
    case _ => false}}
  def peut_boire = {
    this.est_actif || this.danse || this.est_avec_bouclette || this.fume
  }
  // -------- Gerer boure --------
  def get_boure = {m_alcoolemie}
  def set_boure(b : Double) {
    m_alcoolemie = math.max(0., math.min(100.,b));
    if(m_alcoolemie >= 100. && this.peut_boire)
      {set_etat(Etat_coma(get_max_coma))
        add_etudiant_coma(this)
      }
    else if((m_alcoolemie < 100) && (! this.dans_coma) && (! this.est_avec_bouclette))
      get_etat match {
        case Etat_coma(_) => set_etat(Etat_actif())
        case _            => ()
      }
  }
  def add_boure(boure : Double) {set_boure(get_boure + boure)}
  def boire_un_verre(qte_alcool : Double) {
    add_boure(qte_alcool * get_boure_par_verre)
  }
  def se_prendre_un_mur() {add_boure(15)}
  def reprendre_esprit = {set_boure(get_boure - get_reprendre_esprit)}
  // -------- Gere les mouvements --------
  // Quand l'étudiant est bouré, il a une probabilité non nulle de se tromper
  // de case. Cette probabilité est définie ici, entre 0 (ne se trompe pas)
  // et 100 (se trompe toujours)
  // TODO : IL FAUT L'OVERRIDER !!!
  protected var chemin = new Path_astar(Type_soigneur(),coord,coord) //sert pour les soigneurs, calcul instantanné

  def get_chemin = {chemin}
  def set_chemin(type_etud:Type_etudiant,soigneur:(Int,Int),comateux:(Int,Int)) {chemin = new Path_astar(type_etud,soigneur,comateux)}

  def alea_en_fct_de_bouree : Int
  def delta_mini_steps = {
    if (this.dans_coma)
      0
    else
      president.get_delta_mini_steps
  }
  def changer_etat_mini_step {
    val i = president.get_delta_mini_steps
    get_etat match {
      case Etat_danse(n)     => set_etat(Etat_danse(n-i))
      case Etat_fume(n)      => set_etat(Etat_fume(n-i))
      case Etat_bouclette(n) => set_etat(Etat_bouclette(n-i))
      case Etat_coma(n)      => set_etat(Etat_coma(n-i))
      case _                 => ()
    }
  }
  //this.bouger_grand_pas // On s'assure que le joueur bouge dés le début // En fait c'est pas cool, ça peut faire suivre aux monstres un mauvais path
  def bouger_grand_pas {
    val old_coord = get_coord
    this.reprendre_esprit
    get_etat match {
      case Etat_actif()          => bouger_quand_actif
      case Etat_quitte_kokarde() => bouger_quand_actif
      case Etat_danse(_)         => bouger_quand_danse
      case _                     => ()
    }
    // Si on a atteint le but alors on ne va pas plus loin
    // (ça ne se fait pas de monter sur le bar)
    if (maj_but)
      set_next_coord(old_coord)
    // On diminue le temps de pénalité
    m_nb_steps_penalite match {
      case None => ()
      case Some((n,v)) =>
        if (n <= 0)
          enlever_penalite
        else
          set_nb_steps_penalite(n-1,v)
    }
  }
  def bouger_quand_danse {
    val wanted_next_ij =
      kokarde.get_rand.nextInt(4) match {
        case 0 => (get_pos_i,get_pos_j+1)
        case 1 => (get_pos_i-1,get_pos_j)
        case 2 => (get_pos_i,get_pos_j-1)
        case _ => (get_pos_i+1,get_pos_j)
      }
    if (est_accessible_etudiant(this, wanted_next_ij))
      set_next_pos_ij(wanted_next_ij)
  }
  def bouger_quand_actif {
    // TODO : regarder suivant les cas quoi faire : faire pathfinding...
    // C'est aussi ici que l'on utilise la fonction alea_en_fct_de_bouree
    // On bouge case par case.
    val wanted_next_ij =
      get_but match{
        case But_soin(Some(c)) => get_chemin.case_suivante(get_coord)
        case But_soin(None) => get_coord
        case _ =>
      
      if (kokarde.get_rand.nextInt(100) < alea_en_fct_de_bouree)
      { // L'étudiant a trop bu, il marche aléatoirement
        println("=> alea_en_fct_de_bouree")
        kokarde.get_rand.nextInt(4) match {
          case 0 => (get_pos_i,get_pos_j+1)
          case 1 => (get_pos_i-1,get_pos_j)
          case 2 => (get_pos_i,get_pos_j-1)
          case _ => (get_pos_i+1,get_pos_j)}
      }
      else
      { // L'étudiant avance normalement
        val possible_cases = map_path.get_path(this.get_type,this.get_but).cases_suivantes(get_coord)
        if (possible_cases.isEmpty)
          get_coord
        else
        {
          val c2 = possible_cases(kokarde.get_rand.nextInt(possible_cases.length))
          println("=> Case actuelle : ", get_coord)
          println("=> Case suivante : ", c2)
          c2
        }
        // (get_pos_i,get_pos_j+1)
      }
    }
    println("==> Wanted Case : ", wanted_next_ij, kokarde.est_accessible_etudiant(this,wanted_next_ij))
    if (kokarde.est_accessible_etudiant(this,wanted_next_ij))
      set_next_pos_ij(wanted_next_ij)
    else if (est_actif && ! (kokarde.get_cases_but(get_but) contains wanted_next_ij))
      this.se_prendre_un_mur // Quand l'étudiant fonce dans un mur il perd de la vie
    else
      ()
    println("===> Nouvelle case : ", get_next_coord)
  }
  def maj_but : Boolean = {
    // On regarde s'ils ont atteint leur but, et dans ce cas ils
    // changent de direction. Cette fonction renvoit un booléan
    // si elle a effectué un changement.
    println(("====== [maj_but] But Actuel =====", get_but, get_etat))
    get_but match {
      case But_bar() =>
        if (kokarde.est_bar(get_next_pos_ij))
        {
          action_arrive_bar
          set_but(But_DJ())
          true
        }
        else
          false
      case But_fumer() =>
        if (kokarde.est_dehors(get_next_pos_ij))
        {
          action_debut_fume
          set_etat(Etat_fume(6*president.get_nb_caract_mini_steps))
          set_but(But_bar())
          true
        }
        else
          false
      case But_DJ() =>
        if (kokarde.est_dj(get_next_pos_ij))
        {
          action_arrive_dj
          if (kokarde.get_rand.nextInt(100) < get_proba_aller_fumer)
            set_but(But_fumer())
          else
          {
            set_but(But_bar())
            if (kokarde.get_rand.nextInt(100) < get_proba_aller_danser)
              set_etat(Etat_danse(get_temps_danse))
          }
          true
        }
        else
          false
      case But_soin(None) => {
              set_but_soin
              get_but match{case But_soin(None)=> false
                            case _ => true}}
      case But_soin(Some(m_etud)) => 
        if (get_next_pos_ij == m_etud.get_coord)
        {
          set_but(But_soin(None))
          m_etud.soin
          true
        }
        else
          false 
      case _ => false
    }
  }
  // -------- Gere les actions --------
  def action_arrive_bar
  {
    // L'étudiant prends une bierre
    dj.gagne_or(get_or_passage_bar)
    add_boure(get_alcoolemie_passage_bar)
    // Veut aller jeter ça sur le DJ
    set_but(But_DJ())
  }
  def action_arrive_dj
  {
    dj.perdre_vie(get_qte_bierre_transportable)
  }
  def action_quitte_kokarde() {} // TODO
  def action_debut_fume() {}     // TODO
  def action_fin_fume() {}       // TODO
  def action_debut_danse() {}    // TODO
  def action_fin_danse() {}      // TODO
  def action_fin_coma() {}       // TODO
  // ... Si besoin plein d'autres actions...

  def soin
  {
    set_boure(get_boure - 50 * get_reprendre_esprit)
    set_etat(Etat_actif())
    del_etudiant_coma(this)
  }
}
// ----------------------------------------
// ------------  Instances  ---------------
// ----------------------------------------
// On défini ici les étudiants qui pourront
// être envoyés sur le plateau
// Si on ne souhaite pas overrider les fonctions
// principales (bouger...), on créé directement
// un étudiant, sinon on surcharge la classe
// étudiants.

object Etudiant_Premier_annee_usine extends Etudiant_usine {
  def apply(coord:(Int,Int)) = {new Etudiant_Premier_annee(this,coord)}
  var m_nom = "etudiant_premier_annee"
  var m_pretty_nom = "Etudiant en première année"
  var m_default_nb_steps = president.get_nb_caract_mini_steps
  var m_boure_par_verre = 10.
  var m_max_coma = 4*president.get_nb_caract_mini_steps
  var m_reprendre_esprit = 1.
  var m_or_passage_bar = 100
  var m_alcoolemie_passage_bar   = 15.
  var m_qte_bierre_transportable = 10 // En cl
  var m_gain_or_quand_mort = 300
  var m_type : Type_etudiant = Type_basique()
  var m_proba_aller_fumer    = 0
  def get_proba_aller_danser = {30 + (70*dj.get_qualite_musique)/100}
  def get_temps_danse = (3 + dj.get_qualite_musique/10)*president.get_nb_caract_mini_steps
}
class Etudiant_Premier_annee(etudiant_usine:Etudiant_usine,coord:(Int,Int))
    extends Etudiant(etudiant_usine,coord)
{
  def alea_en_fct_de_bouree = get_boure.toInt
}


object Etudiant_Danseurs_usine extends Etudiant_usine {
  def apply(coord:(Int,Int)) = {new Etudiant_Danseurs(this,coord)}
  var m_nom = "etudiant_danseurs"
  var m_pretty_nom = "Danseurs"
  var m_default_nb_steps = (president.get_nb_caract_mini_steps*70)/100
  var m_boure_par_verre = 1.
  var m_max_coma = 4*president.get_nb_caract_mini_steps
  var m_reprendre_esprit = 1.
  var m_or_passage_bar = 100
  var m_alcoolemie_passage_bar   = 0.
  var m_qte_bierre_transportable = 10 // En ml
  var m_gain_or_quand_mort = 800
  var m_type : Type_etudiant = Type_basique()
  var m_proba_aller_fumer    = 0
  def get_proba_aller_danser = 100
  def get_temps_danse        = 10*president.get_nb_caract_mini_steps
}
class Etudiant_Danseurs(etudiant_usine:Etudiant_usine,coord:(Int,Int))
    extends Etudiant(etudiant_usine,coord)
{
  def alea_en_fct_de_bouree = get_boure.toInt
  m_etat = Etat_danse(get_temps_danse)
}


object Etudiant_Fantome_usine extends Etudiant_usine {
  def apply(coord:(Int,Int)) = {new Etudiant_Fantome(this,coord)}
  var m_nom = "etudiant_fantome"
  var m_pretty_nom = "Fantôme"
  var m_default_nb_steps = (president.get_nb_caract_mini_steps*140)/100
  var m_boure_par_verre = 1.
  var m_max_coma = 4*president.get_nb_caract_mini_steps
  var m_reprendre_esprit = 1.
  var m_or_passage_bar = 100
  var m_alcoolemie_passage_bar   = 0.
  var m_qte_bierre_transportable = 10 // En ml
  var m_gain_or_quand_mort = 700
  var m_type : Type_etudiant = Type_fantome()
  var m_proba_aller_fumer    = 0
  def get_proba_aller_danser = {30 + (70*dj.get_qualite_musique)/100}
  def get_temps_danse        = (3 + dj.get_qualite_musique/10)*president.get_nb_caract_mini_steps
}
class Etudiant_Fantome(etudiant_usine:Etudiant_usine,coord:(Int,Int))
    extends Etudiant(etudiant_usine,coord)
{
  def alea_en_fct_de_bouree = get_boure.toInt/2
}


object Etudiant_B0_usine extends Etudiant_usine {
  def apply(coord:(Int,Int)) = {new Etudiant_B0(this,coord)}
  var m_nom = "etudiant_b0"
  var m_pretty_nom = "B0"
  var m_default_nb_steps = president.get_nb_caract_mini_steps
  var m_boure_par_verre = 0.
  var m_max_coma = 10*president.get_nb_caract_mini_steps
  var m_reprendre_esprit = 1.
  var m_or_passage_bar = 1000
  var m_alcoolemie_passage_bar   = 0.
  var m_qte_bierre_transportable = 1000 // En ml
  var m_gain_or_quand_mort = 4000
  var m_type : Type_etudiant = Type_sale()
  var m_proba_aller_fumer    = 50
  def get_proba_aller_danser = {30 + (70*dj.get_qualite_musique)/100}
  def get_temps_danse        = (3 + dj.get_qualite_musique/10)*president.get_nb_caract_mini_steps
}
class Etudiant_B0(etudiant_usine:Etudiant_usine,coord:(Int,Int))
    extends Etudiant(etudiant_usine,coord)
{
  def alea_en_fct_de_bouree = get_boure.toInt/2
}


object Etudiant_Aspique_usine extends Etudiant_usine {
  def apply(coord:(Int,Int)) = {new Etudiant_Aspique(this,coord)}
  var m_nom = "etudiant_aspique"
  var m_pretty_nom = "Aspique"
  var m_default_nb_steps = (president.get_nb_caract_mini_steps*20)/100
  var m_boure_par_verre = 1.
  var m_max_coma = 10*president.get_nb_caract_mini_steps
  var m_reprendre_esprit = 1.
  var m_or_passage_bar = 100
  var m_alcoolemie_passage_bar   = 0.
  var m_qte_bierre_transportable = 100 // En ml
  var m_gain_or_quand_mort = 4000
  var m_type : Type_etudiant = Type_sale()
  var m_proba_aller_fumer    = 0
  def get_proba_aller_danser = 0
  def get_temps_danse        = (3 + dj.get_qualite_musique/10)*president.get_nb_caract_mini_steps
}
class Etudiant_Aspique(etudiant_usine:Etudiant_usine,coord:(Int,Int))
    extends Etudiant(etudiant_usine,coord)
{
  def alea_en_fct_de_bouree = get_boure.toInt/4
}


object Etudiant_Voleur_usine extends Etudiant_usine {
  def apply(coord:(Int,Int)) = {new Etudiant_Voleur(this,coord)}
  var m_nom = "etudiant_voleur"
  var m_pretty_nom = "Voleur"
  var m_default_nb_steps = (president.get_nb_caract_mini_steps*80)/100
  var m_boure_par_verre = 1.
  var m_max_coma = 10*president.get_nb_caract_mini_steps
  var m_reprendre_esprit = 1.
  var m_or_passage_bar = -400
  var m_alcoolemie_passage_bar   = 10.
  var m_qte_bierre_transportable = 10 // En ml
  var m_gain_or_quand_mort = 1000
  var m_type : Type_etudiant = Type_basique()
  var m_proba_aller_fumer    = 30
  def get_proba_aller_danser = 10
  def get_temps_danse        = (3 + dj.get_qualite_musique/10)*president.get_nb_caract_mini_steps
}
class Etudiant_Voleur(etudiant_usine:Etudiant_usine,coord:(Int,Int))
    extends Etudiant(etudiant_usine,coord)
{
  def alea_en_fct_de_bouree = get_boure.toInt/2
}



object Etudiant_cle_trackNart_usine extends Etudiant_usine {
  def apply(coord:(Int,Int)) = {new Etudiant_cle_trackNart(this,coord)}
  var m_nom = "etudiant_cle_trackNart"
  var m_pretty_nom = "Etudiant du BDA"
  var m_default_nb_steps = president.get_nb_caract_mini_steps
  var m_boure_par_verre = 10.
  var m_max_coma = 4*president.get_nb_caract_mini_steps
  var m_reprendre_esprit = 1.
  var m_or_passage_bar = 100
  var m_alcoolemie_passage_bar   = 15.
  var m_qte_bierre_transportable = 10 // En cl
  var m_gain_or_quand_mort = 500
  var m_type : Type_etudiant = Type_cle_trackNart()
  var m_proba_aller_fumer    = 0
  def get_proba_aller_danser = {30 + (70*dj.get_qualite_musique)/100}
  def get_temps_danse = (3 + dj.get_qualite_musique/10)*president.get_nb_caract_mini_steps
}
class Etudiant_cle_trackNart(etudiant_usine:Etudiant_usine,coord:(Int,Int))
    extends Etudiant(etudiant_usine,coord)
{
  def alea_en_fct_de_bouree = get_boure.toInt
}




object Etudiant_gros_fumeur_usine extends Etudiant_usine {
  def apply(coord:(Int,Int)) = {new Etudiant_gros_fumeur(this,coord)}
  var m_nom = "etudiant_gros_fumeur"
  var m_pretty_nom = "Fume beaucoup"
  var m_default_nb_steps = president.get_nb_caract_mini_steps*3
  var m_boure_par_verre = 10.
  var m_max_coma = 10*president.get_nb_caract_mini_steps
  var m_reprendre_esprit = 1.
  var m_or_passage_bar = 100
  var m_alcoolemie_passage_bar   = 15.
  var m_qte_bierre_transportable = 10 // En cl
  var m_gain_or_quand_mort = 200
  var m_type : Type_etudiant = Type_basique()
  var m_proba_aller_fumer    = 100
  def get_proba_aller_danser = 0
  def get_temps_danse = (3 + dj.get_qualite_musique/10)*president.get_nb_caract_mini_steps
}
class Etudiant_gros_fumeur(etudiant_usine:Etudiant_usine,coord:(Int,Int))
    extends Etudiant(etudiant_usine,coord)
{
  def alea_en_fct_de_bouree = get_boure.toInt
  m_etat = Etat_fume(80)
}


object Etudiant_sage_usine extends Etudiant_usine {
  def apply(coord:(Int,Int)) = {new Etudiant_sage(this,coord)}
  var m_nom = "etudiant_sage"
  var m_pretty_nom = "Sans alcool, la fête est plus molle"
  var m_default_nb_steps = president.get_nb_caract_mini_steps
  var m_boure_par_verre = 10.
  var m_max_coma = 0*president.get_nb_caract_mini_steps
  var m_reprendre_esprit = 1.
  var m_or_passage_bar = 0
  var m_alcoolemie_passage_bar   = 0.
  var m_qte_bierre_transportable = 10 // En cl
  var m_gain_or_quand_mort = 700
  var m_type : Type_etudiant = Type_sage()
  var m_proba_aller_fumer    = 1
  def get_proba_aller_danser = {30 + (70*dj.get_qualite_musique)/100}
  def get_temps_danse = (3 + dj.get_qualite_musique/10)*president.get_nb_caract_mini_steps
}
class Etudiant_sage(etudiant_usine:Etudiant_usine,coord:(Int,Int))
    extends Etudiant(etudiant_usine,coord)
{
  def alea_en_fct_de_bouree = get_boure.toInt
}

object Etudiant_soigneur_usine extends Etudiant_usine {
  def apply(coord:(Int,Int)) = {new Etudiant_soigneur(this,coord)}
  var m_nom = "etudiant_soigneur"
  var m_pretty_nom = "Le samu arrive !"
  var m_default_nb_steps = president.get_nb_caract_mini_steps*20/100
  var m_boure_par_verre = 10.
  var m_max_coma = 0*president.get_nb_caract_mini_steps
  var m_reprendre_esprit = 1.
  var m_or_passage_bar = 0
  var m_alcoolemie_passage_bar   = 0.
  var m_qte_bierre_transportable = 10 // En cl
  var m_gain_or_quand_mort = 5000
  var m_type : Type_etudiant = Type_soigneur()
  var m_proba_aller_fumer    = 0
  def get_proba_aller_danser = 0
  def get_temps_danse = 0
}
class Etudiant_soigneur(etudiant_usine:Etudiant_usine,coord:(Int,Int))
    extends Etudiant(etudiant_usine,coord)
{
  def alea_en_fct_de_bouree = 0
  set_but(But_soin(None))
}




