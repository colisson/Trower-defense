package kokarde_defense

// ========================================
// =============  Niveaux  ================
// ========================================
// Génération automatique de vagues aléatoires de monstres.

import scala.collection.mutable._
import scala.io.Source
import java.util.Random
import scala.util.parsing.combinator._
import Array._



class SimpleParser extends RegexParsers {
  def word: Parser[String]   = """([A-Z]|[a-z]|[0-9]|\_)+""".r       ^^ { _.toString }
  def number: Parser[String]    = """(0|[1-9]\d*)""".r ^^ { _.toString }
  def misc: Parser[String]    = """\(|,|\)""".r ^^ { _.toString }
  def monstre: Parser[Array[String]] = word ~ number ~ misc ~ number ~ misc ~ number ~ misc       ^^ { case nom ~ proba ~ m1 ~ inf ~ m2 ~ sup ~ m3 => Array(nom,proba,inf,sup) }
  }

object ParseurDeNiveaux extends SimpleParser {
  def parsage_niveaux(nomFichier:String,numeroNiveau:Int):kokarde_defense.Niveau = {
    var coefficient: Int = 1
    if (numeroNiveau == 3) { coefficient = 2 }
    val rand = new Random(System.currentTimeMillis());
    val lines = Source.fromInputStream(getClass.getResourceAsStream(nomFichier)).getLines().toList
    var deja_commence: Boolean = false
    var nouvelle_vague: Int = 0
    var monstres: Array[String]  = Array()
    var maximum: Int = 257
    var indice_maximum: Int = 0
    var niveau_debute: Boolean = false
    var niveau = new Niveau(0,List())
    numeroNiveau match {
      case 1 => { niveau = new Niveau(10000, List(Membre_bde_Glacons_usine,Membre_bde_Marchant_sable_usine,Membre_bde_Bouclette_usine,Membre_bde_Vigile_usine)) }
      case 2 => { niveau = new Niveau(0, List(Membre_bde_Glacons_usine,Membre_bde_Marchant_sable_usine,Membre_bde_Bouclette_usine,Membre_bde_Vigile_usine,Membre_bde_Chtulu_usine,Membre_bde_Communiste_usine)) }
      }
    lines.zipWithIndex.map{ case (line,i) =>
      if (nouvelle_vague == 1) {
        // on a fini une vague, il faut mettre à jour la variable niveau
        if (niveau_debute == true) { // s'il y a bien eu une vague avant
          var vague = new Vague()
          val nb_monstres: Int = monstres.length
          // on fait en quelque sorte une permutation
          var deja_vu = new Array[Boolean](nb_monstres)
          var aleatoire = new Array[Int](nb_monstres)
          for (i <- 0 to nb_monstres - 1) {
            deja_vu(i) = false
            aleatoire(i) = rand.nextInt(256)
            }
          var monstres_permutes: Array[String]  = Array()
          for (i <- 0 to nb_monstres - 1 ) {
            maximum = -1
            indice_maximum = -1 
            for (j <- 0 to nb_monstres - 1 ) {
              if (aleatoire(j) > maximum && deja_vu(j) == false) {
                  maximum = aleatoire(j)
                  indice_maximum = j 
                }
              }
            monstres_permutes = concat(monstres_permutes, Array(monstres(indice_maximum)))
            deja_vu(indice_maximum) = true
            }
          for (x <- monstres_permutes) {
            x match { // on crée les vagues
              case "gros_fumeur" => { vague.ajouter_flot(List((None,coefficient,Etudiant_gros_fumeur_usine))) }
              case "Premier_annee" => { vague.ajouter_flot(List((None,coefficient,Etudiant_Premier_annee_usine))) }
              case "Danseurs" => { vague.ajouter_flot(List((None,coefficient,Etudiant_Danseurs_usine))) }
              case "Fantome" => { vague.ajouter_flot(List((None,coefficient,Etudiant_Fantome_usine))) }
              case "Aspique" => { vague.ajouter_flot(List((None,coefficient,Etudiant_Aspique_usine))) }
              case "cle_trackNart" => { vague.ajouter_flot(List((None,coefficient,Etudiant_cle_trackNart_usine))) }
              case "B0" => { vague.ajouter_flot(List((None,coefficient,Etudiant_B0_usine))) }
              case "sage" => { vague.ajouter_flot(List((None,coefficient,Etudiant_sage_usine))) }
              case "Voleur" => { vague.ajouter_flot(List((None,coefficient,Etudiant_Voleur_usine))) }
              }
            }
          niveau.ajouter_vague(vague)
          }
        monstres = Array()
        nouvelle_vague = nouvelle_vague - 1
        }
      deja_commence = false
      niveau_debute = true
      var remaining: String = line
      var continuer: Boolean = true
      while (continuer) {
        parse(monstre, remaining) match {
          case Success(matched,next) => {
              // 1) est-ce que le monstre va apparaître ?
              val nombre_aleatoire = rand.nextInt(100)
              if (nombre_aleatoire < matched(1).toInt) {
                // si oui, combien de fois ?
                val nombre_de_fois = rand.nextInt(1 + matched(3).toInt-matched(2).toInt) + matched(2).toInt
                for (i <- 1 to nombre_de_fois) {
                  monstres = concat(monstres, Array(matched(0)))
                  }
                }
              remaining = next.source.toString.substring(next.pos.column-1)
              deja_commence = true
              }
          case Failure(msg,_) => { continuer = false; if (deja_commence == false) { nouvelle_vague = nouvelle_vague + 1 }}
          case Error(msg,_) => { continuer = false; if (deja_commence == false) { nouvelle_vague = nouvelle_vague + 1 }}
          }
        }
      }
    niveau
    }
  } 
