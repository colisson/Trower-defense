package kokarde_defense

// ========================================
// =========== Icones et images ===========
// ========================================
// La génération d'images et d'icones
// redimensionnées est relativement longue
// à calculer. Afin de diminuer ce temps
// il est intéressant de stoquer les images
// ayant les bonnes dimensions dans des tables
// de hashage.

// http://www.scala-lang.org/api/current/#scala.collection.mutable.HashMap

import swing._
import swing.event._
import javax.swing.ImageIcon
import java.awt.Dimension

import scala.swing._
import scala.swing.BorderPanel.Position._
import java.awt.{ Color, Graphics2D }
import scala.util.Random
import scala.swing.Panel
import javax.swing.ImageIcon
import java.awt.image.BufferedImage
import java.awt.image._
import javax.swing.Icon
import java.awt.Image
import java.io.File
import java.nio.file.{Paths, Files}
import javax.imageio.ImageIO
import java.awt.color.ColorSpace
import scala.collection.mutable.HashMap

object svg_images {
  protected val hash_icon = HashMap.empty[String,ImageIcon]
  def icon_of_file(filename:String) : ImageIcon = {
    hash_icon.get(filename) match
    {
      case Some(i) => i
      case None =>
        val icon_file = getClass.getResource(filename)
        val icon =
          if (icon_file != null) {new ImageIcon(icon_file)}
          else {
            println("/!\\Pas d'icone pour :")
            println(filename)
            new ImageIcon(getClass.getResource("chose_inconnue.png"))
          }
        hash_icon += (filename -> icon)
        icon
    }
  }
  protected val hash_image = HashMap.empty[String,BufferedImage]
  def image_of_file(filename:String) : BufferedImage = {
    hash_image.get(filename) match
    {
      case Some(i) => i
      case None =>
        val img_file = getClass.getResource(filename)
        val buff_img : BufferedImage =
          if (img_file != null) {ImageIO.read(img_file)}
          else {
            println("/!\\Pas d'icone pour :")
            println(filename)
            ImageIO.read(getClass.getResource("chose_inconnue.png"))}
        hash_image += (filename -> buff_img)
        buff_img
    }
  }
  protected val hash_resized_icon = HashMap.empty[(String,Int,Int),ImageIcon]
  def resized_icon_of_file(filename:String,width:Int,height:Int):ImageIcon = {
    hash_resized_icon.get((filename,width,height)) match
    {
      case Some(i) => i
      case None =>
        val icon = icon_of_file(filename)
        val img = icon.getImage()
        val newImage = img.getScaledInstance(width,
          height,
          java.awt.Image.SCALE_SMOOTH)
        val new_icon = new ImageIcon(newImage)
        hash_resized_icon += ((filename,width,height) -> new_icon)
        new_icon
    }
  }
  protected val hash_resized_image = HashMap.empty[(String,Int,Int),BufferedImage]
  def resized_image_of_file(filename:String,width:Int,height:Int):BufferedImage = {
    hash_resized_image.get((filename,width,height)) match
    {
      case Some(i) => i
      case None =>
        val icon = resized_icon_of_file(filename,width,height)
        val image = icon.getImage()
        val newImage = new BufferedImage(width, height,
          BufferedImage.TYPE_INT_ARGB)
        val g = newImage.getGraphics()
        g.drawImage(image, 0, 0, null);
        g.dispose();
        hash_resized_image += ((filename,width,height) -> newImage)
        newImage
    }
  } 
  protected val hash_resized_image_grayscale = HashMap.empty[(String,Int,Int),BufferedImage]
  def resized_image_grayscale_of_file(filename:String,width:Int,height:Int):BufferedImage = {
    hash_resized_image_grayscale.get((filename,width,height)) match
    {
      case Some(i) => i
      case None =>
        val icon = resized_icon_of_file(filename,width,height)
        val image = icon.getImage()
        val newImage = new BufferedImage(width, height,
          BufferedImage.TYPE_INT_ARGB)
        val g = newImage.getGraphics()
        g.drawImage(image, 0, 0, null);
        g.dispose();
        val op : ColorConvertOp = new ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_GRAY), null)
        op.filter(newImage, newImage)
        hash_resized_image_grayscale += ((filename,width,height) -> newImage)
        newImage
    }
  } 
  protected val hash_resized_icon_grayscale = HashMap.empty[(String,Int,Int),ImageIcon]
  def resized_icon_grayscale_of_file(filename:String,width:Int,height:Int):ImageIcon = {
    hash_resized_icon_grayscale.get((filename,width,height)) match
    {
      case Some(i) => i
      case None =>
        val newImage = resized_image_grayscale_of_file(filename,width,height)
        val newIcon = new ImageIcon(newImage)
        hash_resized_icon_grayscale += ((filename,width,height) -> newIcon)
        newIcon
    }
  } 
}
