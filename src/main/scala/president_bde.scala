package kokarde_defense
import ParseurDeNiveaux._
// ========================================
// ==========  Président BDE  =============
// ========================================
// Ce fichier (équivalent de l'ancien cerveau.scala)
// permet de gérer les interractions entre les objets
// du jeux et la partie graphique. Il gère également
// tout ce qui se rapporte aux niveaux.

// ----------------------------------------
// -----------  Les niveaux  --------------
// ----------------------------------------
// Ici on gère tout ce qui se rapporte aux niveaux
// Chaque niveau contient un certain nombre de
// vagues de montres. Entre chaque vague le joueur
// a du temps pour pouvoir positionner ses tours
// Les vagues elles même sont composées de flots
// de monstres qui arrivent tous les 100 mini_steps.
class Vague() {
  protected var m_flots_etudiants : List[List[Etudiant]] = List()
  def get_flots_etudiants = {m_flots_etudiants}
  def reste_flot(t:Int) = {t < m_flots_etudiants.length}
  def get_flot(t:Int) = {
    if(reste_flot(t))
      m_flots_etudiants(t)
    else
      List()
  }
  // Position, nombre, echantillon du monstre
  // On renvoit ici l'objet pour pouvoir chaîner les ajouter_flot
  // La liste contient une option pour pouvoir dire "si non précisé,
  // prends une case de la Map au hasard". Le deuxième int est pour le
  // nombre d'étudiants.
  def ajouter_flot(lst_etudiants : List[(Option[(Int,Int)],Int,Etudiant_usine)]):Vague = {
    val flot = lst_etudiants.map{case (coord_opt,nb_etudiants,usine) =>
      val coord : (Int,Int) = coord_opt match {
        case Some(coord) => coord
        case None => kokarde.get_random_case_depart};
      (0 until nb_etudiants).toList.map(_ => usine(coord))
    }.flatten
    m_flots_etudiants = m_flots_etudiants ++ List(flot)
    Vague.this
  }
}

class Niveau(default_add_or : Int, membres_bde_constructibles : List[Membre_bde_usine])
{
  protected var m_l_vague : List[Vague] = List()
  // On peut éventuellement ajouter à chaque nouveau niveau
  // un peu d'or au joueur
  protected val m_default_add_or = default_add_or
  protected val m_membres_bde_constructibles = membres_bde_constructibles
  def get_default_add_or = {m_default_add_or}
  def get_membres_bde_constructibles = {m_membres_bde_constructibles}

  def ajouter_vague(v : Vague) : Niveau =
  {
    m_l_vague = m_l_vague ++ List(v)
    Niveau.this
  }
  def reste_vague(t:Int) = {t < m_l_vague.length}
  def get_vague(t:Int) = {m_l_vague(t)}
}

class Tous_les_niveaux()
{
  protected var m_l_nvx : List[Niveau] = List()
  def reset_niveau {m_l_nvx = List()}
  def ajouter_niveau(n : Niveau) : Tous_les_niveaux =
  {
    m_l_nvx = m_l_nvx ++ List(n)
    Tous_les_niveaux.this
  }
  def reste_niveau(t:Int) = {t < m_l_nvx.length}
  def get_niveau(t:Int) = {m_l_nvx(t)}
}

// ----------------------------------------
// -----------  Le président  -------------
// ----------------------------------------
// C'est ici que l'on gère les mécanismes principaux
abstract class Etat
case class Positionner_membres_bde extends Etat
case class Mini_steps(n:Int) extends Etat // Tous les 100 mini
case class Perdu             extends Etat
case class Gagne             extends Etat

object president {
  protected var m_etat           : Etat = Positionner_membres_bde()
  protected var m_current_niveau : Int  = 0
  protected var m_current_vague  : Int  = 0
  protected var m_current_flot   : Int  = 0
  protected var m_tous_les_niveaux = new Tous_les_niveaux()
  // --- La vitesse de jeux ---
  // Nombre caractéristique de mini steps pour parcourir une case
  // (peut varier suivant le joueur etc...)
  protected var m_nb_caract_mini_steps : Int = 20
  // A chaque tour d'horloge on bouge de n mini steps :
  protected var m_delta_mini_steps = 1
  this.recommencer_partie
  // -------- Gérer la vitesse de jeux --------
  def get_nb_caract_mini_steps = {m_nb_caract_mini_steps}
  def get_delta_mini_steps = {m_delta_mini_steps}
  // -------- Gérer les etats du jeux et niveaux --------
  def get_etat = { m_etat }
  def set_etat(e:Etat) { m_etat = e}
  def get_current_niveau = { m_current_niveau }
  def set_current_niveau(n:Int) { m_current_niveau = n }
  def get_current_vague = { m_current_vague }
  def set_current_vague(v:Int) { m_current_vague = v }
  def get_current_flot = { m_current_flot }
  def set_current_flot(v:Int) { m_current_flot = v }
  // Sert à passer au niveau suivant, éventuellement 
  def maj_niveau() {
    val niv = m_tous_les_niveaux.get_niveau(m_current_niveau)
    dj.gagne_or(niv.get_default_add_or)
    m_current_flot  = 0
    m_current_vague = 0
  }
  // -------- Gérer les membres_bde que l'on peut construire --------
  def get_membres_bde_constructibles = {
    if(m_tous_les_niveaux.reste_niveau(m_current_niveau))
      m_tous_les_niveaux.get_niveau(m_current_niveau).get_membres_bde_constructibles
    else
      List()
  }
  def recommencer_partie {
    // On réinitialise le DJ et kokarde
    dj.reinitialiser
    kokarde.reinitialiser
    // On crée tous les niveaux
    m_tous_les_niveaux.reset_niveau
    // TODO : ajouter les niveaux
    /* val nvx_1 = new Niveau(30000, List(Membre_bde_Glacons_usine,Membre_bde_Vigile_usine,Membre_bde_Communiste_usine,Membre_bde_Bouclette_usine))
    nvx_1.ajouter_vague(
      new Vague()
        .ajouter_flot(List((None,1,Etudiant_gros_fumeur_usine))))
      .ajouter_vague(
      new Vague()
        .ajouter_flot(List((None,1,Etudiant_cle_trackNart_usine)))
        .ajouter_flot(List((None,1,Etudiant_Premier_annee_usine)))
        .ajouter_flot(List((None,1,Etudiant_Premier_annee_usine)))
        .ajouter_flot(List((None,1,Etudiant_Danseurs_usine)))
        .ajouter_flot(List((None,1,Etudiant_Fantome_usine)))
        .ajouter_flot(List((None,1,Etudiant_gros_fumeur_usine))))
      .ajouter_vague(
      new Vague()
        // .ajouter_flot(List((None,1,Etudiant_Premier_annee_usine)))
        .ajouter_flot(List((None,1,Etudiant_cle_trackNart_usine)))
        .ajouter_flot(List((None,1,Etudiant_Premier_annee_usine)))
        .ajouter_flot(List((None,1,Etudiant_Premier_annee_usine)))
        .ajouter_flot(List((None,1,Etudiant_gros_fumeur_usine)))
        .ajouter_flot(List((None,1,Etudiant_Voleur_usine)))
        .ajouter_flot(List((None,1,Etudiant_Voleur_usine)))
        .ajouter_flot(List((None,1,Etudiant_Voleur_usine)))
        // .ajouter_flot(List((None,1,Etudiant_Danseurs_usine)))
        // .ajouter_flot(List((None,2,Etudiant_Premier_annee_usine)))
    ) 
    m_tous_les_niveaux.ajouter_niveau(nvx_1) */
    m_tous_les_niveaux.ajouter_niveau(parsage_niveaux("niveau1.txt",1))
    m_tous_les_niveaux.ajouter_niveau(parsage_niveaux("niveau2.txt",2))
    //m_tous_les_niveaux.ajouter_niveau(parsage_niveaux("niveau3.txt",3))
    /* val nvx_2 = new Niveau(30000, List(Membre_bde_Bouclette_usine,Membre_bde_Chtulu_usine,Membre_bde_Glacons_usine,Membre_bde_Vigile_usine,Membre_bde_Communiste_usine,Membre_bde_Marchant_sable_usine,Membre_bde_Pasta_usine))
    nvx_2.ajouter_vague(
      new Vague()
        .ajouter_flot(List((None,1,Etudiant_Premier_annee_usine)))
        .ajouter_flot(List((None,1,Etudiant_cle_trackNart_usine)))
        .ajouter_flot(List((None,1,Etudiant_Aspique_usine)))
        .ajouter_flot(List((None,1,Etudiant_Premier_annee_usine)))
        .ajouter_flot(List((None,1,Etudiant_Premier_annee_usine)))
        .ajouter_flot(List((None,1,Etudiant_Premier_annee_usine)))
        .ajouter_flot(List((None,1,Etudiant_Danseurs_usine)))
        .ajouter_flot(List((None,2,Etudiant_Premier_annee_usine))))
      .ajouter_vague(
      new Vague()
        .ajouter_flot(List((None,1,Etudiant_Premier_annee_usine)))
        .ajouter_flot(List((None,1,Etudiant_cle_trackNart_usine)))
        .ajouter_flot(List((None,1,Etudiant_Aspique_usine)))
        .ajouter_flot(List((None,1,Etudiant_Aspique_usine)))
        .ajouter_flot(List((None,1,Etudiant_sage_usine)))
        .ajouter_flot(List((None,1,Etudiant_B0_usine)))
        .ajouter_flot(List((None,1,Etudiant_gros_fumeur_usine)))
        .ajouter_flot(List((None,1,Etudiant_gros_fumeur_usine)))
        .ajouter_flot(List((None,1,Etudiant_gros_fumeur_usine)))
        .ajouter_flot(List((None,1,Etudiant_gros_fumeur_usine)))
        .ajouter_flot(List((None,1,Etudiant_Danseurs_usine)))
        .ajouter_flot(List((None,2,Etudiant_Fantome_usine)))
    )

    m_tous_les_niveaux.ajouter_niveau(nvx_2) */

    m_current_flot   = 0
    m_current_vague  = 0
    m_current_niveau = 0
    this.maj_niveau
    m_etat = Positionner_membres_bde()
  }

  def etape_suivante() =
  {
    m_etat match {
      case Perdu() =>
        (m_etat, "Vous avez perdu.")
      case Gagne() =>
        (m_etat, "Vous avez gagné.")
      case Positionner_membres_bde() =>
        m_etat = Mini_steps(0);
        (m_etat, "")
      case Mini_steps(current_mini_step)  =>
        next_mini_steps(current_mini_step)
    }
  }
  // Ces fonctions sont appellées par etape_suivante()
  def next_mini_steps(current_mini_step:Int) =
  {
    // On regarde si le joueur a gagné
    if (m_tous_les_niveaux.reste_niveau(m_current_niveau))
      il_reste_des_niveaux(current_mini_step,m_tous_les_niveaux.get_niveau(m_current_niveau))
    else
    {
      // On n'est plus dans un niveau : le joueur a gagné !
      m_etat = Gagne()
      (m_etat, "Vous avez gagné !")
    }
  }

  def il_reste_des_niveaux(current_mini_step:Int,niv:Niveau) =
  {
    // On est bien dans un niveau
    if(niv.reste_vague(m_current_vague))
      les_vagues_ne_sont_pas_finies(current_mini_step,niv,niv.get_vague(m_current_vague))
    else
    {
      // On passe au niveau suivant
      m_current_vague   = 0
      m_current_flot    = 0
      m_current_niveau += 1
      if(m_tous_les_niveaux.reste_niveau(m_current_niveau))
      {
        maj_niveau
        m_etat = Positionner_membres_bde()
        (m_etat, "Vous avez remporté ce niveau, niveau suivant !")
      }
      else
      {
        // On a fini tous les niveaux ! (Evite duplicata de code)
        m_etat = Mini_steps(current_mini_step+1)
        (m_etat, "")
      }
    }
  }

  def les_vagues_ne_sont_pas_finies(current_mini_step:Int,niv:Niveau,vague:Vague) =
  {
    // On n'a pas encore fini le niveau, on recharge alors les membres_bde
    kokarde.get_membres_bde.foreach{_.recharger}
    // Les membres_bde tirent (on tire au début pour ne pas tuer les
    // monstres avant leur apparition)
    kokarde.tirs_des_membres_bde
    kokarde.bouger_projectiles
    //  On bouge les monstres (même remarque)
    kokarde.deplacer_etudiants
    // On regarde si on peut lancer des monstres
    val encore_des_flots = vague.reste_flot(m_current_flot)
    // On lance des monstres
    if(encore_des_flots && (current_mini_step % m_nb_caract_mini_steps == 0))
      vague.get_flot(m_current_flot).foreach{m => kokarde.ajouter_etudiant(m)}
    // Tant qu'il reste des monstres ou que des flots sont prévus
    // on continue la simulation
    if (encore_des_flots || kokarde.get_etudiants.length > 0)
    {
      // On teste si le joueur est encore vivant
      if (dj.est_vivant)
      {
        // On passe au flot suivant si c'est le moment
        if (current_mini_step % m_nb_caract_mini_steps == 0)
          m_current_flot += 1
        m_etat = Mini_steps(current_mini_step + 1)
        (m_etat, "")
      }
      else
      {
        m_etat = Perdu()
        (m_etat, "Tu as perdu...")
      }
    }
    else
    {
      // ... sinon on peut dire à l'utilisateur
      // que c'est à lui de jouer
      m_current_vague += 1
      m_current_flot   = 0
      if (niv.reste_vague(m_current_vague))
      {
        m_etat = Positionner_membres_bde()
        (m_etat, "A toi de jouer !")
      }
      else
      {
        // On passe au niveau suivant, pour ne pas dupliquer le code
        // on renvoit un petit pas
        m_etat = Mini_steps(current_mini_step + 1)
        (m_etat, "")
      }
    }
  }
}
