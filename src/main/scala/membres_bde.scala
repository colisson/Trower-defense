package kokarde_defense

// ========================================
// ===========  Membres BDE  ==============
// ========================================
// Le but du BDE est de protéger le DJ
// Il est ainsi composé de divers éléments
// intuitivement vu comme des tours.
// Ils sont amenés à tirer des projectiles
// Pour l'instant ils ne sont pas censés bouger
// mais à terme il est possible que certaines
// tours soient amenées à bouger

// Idées
// Zombie avec des bracelets
// Bombes

import kokarde._
// Les Map sont une bonne alternative entre les listes
// (que l'on peut mapper) et les tableaux (accessibilité
// en temps constant)
import scala.collection.mutable._
import math._
import java.util.Random


abstract class Strategie_bde
case class Strategie_premier extends Strategie_bde
case class Strategie_tous extends Strategie_bde
case class Strategie_un_au_hasard extends Strategie_bde
case class Strategie_alea(n:Int) extends Strategie_bde

abstract class Membre_bde_usine extends Nameable {
  // Pour créer simplement un nouveau membre BDE
  def apply(coord:(Int,Int)) : Membre_bde
  protected val m_description : String
  // Caractéristiques de ce membre
  protected var m_prix : Int
  protected var m_prix_revente : Int
  protected var m_prix_amelioration : Int
  protected var m_amelioration_bde : Option[Membre_bde_usine]
  // Cadence en ministep
  protected var m_cadence_de_tir : Int
  protected var m_projectile : Projectile_usine
  protected var m_strategie : Strategie_bde
  // Portee en case
  protected var m_portee : Double
  // -------- Getter --------
  def get_prix = {m_prix}
  def get_prix_revente = {m_prix_revente}
  def get_prix_amelioration = {m_prix_amelioration}
  def get_amelioration_bde = {m_amelioration_bde}
  def get_cadence_de_tir = {m_cadence_de_tir}
  def get_projectile = {m_projectile}
  def get_strategie = {m_strategie}
  def get_portee = {m_portee}
}
// Il serait trop long de tout défini les attributs en argument, on choisit
// donc de les redéfinir dans les sous classes
abstract class Membre_bde(bde_usine:Membre_bde_usine,coord:(Int,Int)) extends Movable(1,coord,coord)
{
  protected val m_membre_bde_usine : Membre_bde_usine = bde_usine
  // Ici le bool est inutile, on veut seulement savoir si
  // la case est dans la map, c'est tout
  protected var m_temps_avant_tir : Int = 0
  protected var m_cases_accessibles:Map[(Int,Int), Boolean] = Map()
  this.initialiser_cases_accessibles
  // -------- Informations fondamentales --------
  def get_usine = {m_membre_bde_usine}
  def get_nom = {m_membre_bde_usine.get_nom}
  def get_pretty_nom = {m_membre_bde_usine.get_pretty_nom}
  def get_prix = {m_membre_bde_usine.get_prix}
  def get_prix_revente = {m_membre_bde_usine.get_prix_revente}
  def get_prix_amelioration = {m_membre_bde_usine.get_prix_amelioration}
  def get_amelioration_bde = {m_membre_bde_usine.get_amelioration_bde}
  def get_cadence_de_tir = {m_membre_bde_usine.get_cadence_de_tir}
  def get_projectile = {m_membre_bde_usine.get_projectile}
  def get_strategie = {m_membre_bde_usine.get_strategie}
  def get_portee = {m_membre_bde_usine.get_portee}
  // -------- Gérer les cases accessibles --------
  def ajouter_case_accessible(coord:(Int,Int)) = {
    m_cases_accessibles += (coord -> true) }
  def initialiser_cases_accessibles {
    val big_portee = math.ceil(get_portee).toInt
    for {i <- (m_pos_i - big_portee) to (m_pos_i + big_portee)
      j <- (m_pos_j - big_portee) to (m_pos_j + big_portee)}
      if ((pow(m_pos_i - i,2) + pow(m_pos_j - j,2)).toDouble <= (get_portee * get_portee))
        ajouter_case_accessible((i,j))
  }
  // -------- Gérer les tirs --------
  def get_temps_avant_tir = {m_temps_avant_tir}
  protected var m_rand = new Random(System.currentTimeMillis())
  def set_temps_avant_tir(t:Int) {m_temps_avant_tir = t}
  def peut_tirer = {m_temps_avant_tir <= 0}
  // On peut se créer une fonction spéciale pour n'attaquer que
  // certains types d'étudiants, il suffit de surcharger cette fonction
  def filtrer_etudiant(e:Etudiant) = {true}
  def filtrer_etat(e:Etudiant) = {e.filtrer_non_tirable }
  def recharger {m_temps_avant_tir -= 1}
  def tirer {
    if (!peut_tirer)
      recharger
    else
    {
      // On ne prends que les étudiants accessibles
      // val all_et_tmp : Map[(Int,Int),Option[Etudiant]]=
      // m_cases_accessibles.map(x => kokarde.etudiant_present(x._1))
      // val all_et = all_et_tmp.collect({case (k, Some(n)) => (k,n)})
      val all_et = m_cases_accessibles.map(x => kokarde
        .etudiant_present(x._1))
        .collect({case Some(n) => n})
        .filter{e => (this.filtrer_etat(e)) && (this.filtrer_etudiant(e))}
      // .collect({case (coord, Some(etudiant)) => (coord,etudiant)})
      // On choisit une liste d'étudiants à viser
      val selected_et = get_strategie match {
        case Strategie_premier() => all_et.take(1)
        case Strategie_tous() => all_et
        case Strategie_un_au_hasard() => 
          val taille = all_et.size
          all_et.take(m_rand.nextInt(taille)+1).takeRight(1)
        case Strategie_alea(n) =>
          val taille = all_et.size // TODO : rendre plus beau avec -=
          var et_choisis:Array[Etudiant] = Array()
          var ind_choisis:Array[Int] = Array()
          var temp:Int = m_rand.nextInt(taille)+1
          var temp2:Array[Etudiant] = Array()
          for (i <- 0 to n-1) { 
              while (ind_choisis.contains(temp)) { temp = m_rand.nextInt(taille)+1 }
              all_et.take(temp).takeRight(1).copyToArray(temp2)
              et_choisis = et_choisis ++ temp2
              ind_choisis = Array.concat(ind_choisis,Array(temp))
          } 
          et_choisis.toList
      }
      // On tire sur l'étudiant
      selected_et.foreach {case et =>
        kokarde.ajouter_projectile(get_projectile(this.get_coord,et))
        // On réinitialise le temps de tir
        this.set_temps_avant_tir(this.get_cadence_de_tir)
      }
    }
  }
  // -------- Gérer le mouvement --------
  // Pour l'instant personne ne bouge ( TODO )
  def bouger_grand_pas() {}
  def delta_mini_steps() = {president.get_nb_caract_mini_steps}
  def changer_etat_mini_step() = {}


  
}


// ----------------------------------------------------
// -----------  Membres BDE : instances  --------------
// ----------------------------------------------------

// ____ Vigiles ____
object Membre_bde_Vigile_usine extends Membre_bde_usine
{
  def apply(coord : (Int,Int)) = new Membre_bde_Vigile(this,coord)
  var m_nom = "membre_bde_vigile"
  var m_pretty_nom = "Vigile"
  val m_description = "Ce vigile peut donner des coups de poing qu'aucun étudiant ne pourra éviter ! "
  var m_prix = 5000 // Prix en centimes d'euros
  var m_prix_revente = 3000
  var m_prix_amelioration = 20000
  var m_amelioration_bde : Option[Membre_bde_usine] = Some(Membre_bde_Chef_vigile_usine)
  var m_cadence_de_tir = 2 * president.get_nb_caract_mini_steps
  var m_projectile : Projectile_usine = proj_Coup_de_poing_usine
  var m_strategie : Strategie_bde = Strategie_premier()
  var m_portee = 1.5
}
class Membre_bde_Vigile(bde_usine:Membre_bde_usine,coord:(Int,Int))
    extends Membre_bde(bde_usine,coord)
{}

// ____ Chef Vigiles ____
object Membre_bde_Chef_vigile_usine extends Membre_bde_usine
{
  def apply(coord : (Int,Int)) = new Membre_bde_Chef_vigile(this,coord)
  var m_nom = "membre_bde_chef_vigile"
  var m_pretty_nom = "Chef Vigile"
  val m_description = "Il suffit d'un coup de poing pour terrasser n'importe quel étudiant..."
  var m_prix = 20000 // Prix en centimes d'euros
  var m_prix_revente = 12000
  var m_prix_amelioration = 0
  var m_amelioration_bde : Option[Membre_bde_usine] = None
  var m_cadence_de_tir = 2 * president.get_nb_caract_mini_steps
  var m_projectile : Projectile_usine = proj_Coup_de_poing_mortel_usine
  var m_strategie : Strategie_bde = Strategie_premier()
  var m_portee = 1.5
}
class Membre_bde_Chef_vigile(bde_usine:Membre_bde_usine,coord:(Int,Int))
    extends Membre_bde(bde_usine,coord)
{}

// ____ Glaçons ____
object Membre_bde_Glacons_usine extends Membre_bde_usine
{
  def apply(coord : (Int,Int)) = new Membre_bde_Glacon(this,coord)
  var m_nom = "membre_bde_glacons"
  var m_pretty_nom = "Vendeur de glaçons"
  val m_description = "Quand on n'a que des glaçons sous la main..."
  var m_prix = 1000 // Prix en centimes d'euros
  var m_prix_revente = 600
  var m_prix_amelioration = 0
  var m_amelioration_bde : Option[Membre_bde_usine] = None
  var m_cadence_de_tir = president.get_nb_caract_mini_steps*3
  var m_projectile : Projectile_usine = proj_Glacon_usine
  var m_strategie : Strategie_bde = Strategie_premier()
  var m_portee = 3.
}
class Membre_bde_Glacon(bde_usine:Membre_bde_usine,coord:(Int,Int))
    extends Membre_bde(bde_usine,coord)
{}



// ____ Bombe ____
object Membre_bde_Communiste_usine extends Membre_bde_usine
{
  def apply(coord : (Int,Int)) = new Membre_bde_Communiste(this,coord)
  var m_nom = "membre_bde_communiste"
  var m_pretty_nom = "Communiste"
  val m_description = "Les cocktails molotov sont très pratiques pour faire des dégats de zone..."
  var m_prix = 20000 // Prix en centimes d'euros
  var m_prix_revente = 12000
  var m_prix_amelioration = 0
  var m_amelioration_bde : Option[Membre_bde_usine] = None
  var m_cadence_de_tir = president.get_nb_caract_mini_steps*5
  var m_projectile : Projectile_usine = proj_Cocktail_molotov_usine
  var m_strategie : Strategie_bde = Strategie_premier()
  var m_portee = 3.
}
class Membre_bde_Communiste(bde_usine:Membre_bde_usine,coord:(Int,Int))
    extends Membre_bde(bde_usine,coord)
{}


// ____ Marchant de sable ____
object Membre_bde_Marchant_sable_usine extends Membre_bde_usine
{
  def apply(coord : (Int,Int)) = new Membre_bde_Marchant_sable(this,coord)
  var m_nom = "membre_bde_marchant_sable"
  var m_pretty_nom = "Marchant de sable"
  val m_description = "Le marchant de sable ralentit les monstres..."
  var m_prix = 3000 // Prix en centimes d'euros
  var m_prix_revente = 1800
  var m_prix_amelioration = 0
  var m_amelioration_bde : Option[Membre_bde_usine] = None
  var m_cadence_de_tir = president.get_nb_caract_mini_steps*5
  var m_projectile : Projectile_usine = proj_Coup_de_fatigue_usine
  var m_strategie : Strategie_bde = Strategie_premier()
  var m_portee = 3.
}
class Membre_bde_Marchant_sable(bde_usine:Membre_bde_usine,coord:(Int,Int))
    extends Membre_bde(bde_usine,coord)
{}


// ____ Bouclette ____
object Membre_bde_Bouclette_usine extends Membre_bde_usine
{
  def apply(coord : (Int,Int)) = new Membre_bde_Bouclette(this,coord)
  var m_nom = "membre_bde_bouclette"
  var m_pretty_nom = "Bouclette"
  val m_description = "Bouclette ralentit les monstres, à sa manière..."
  var m_prix = 4000 // Prix en centimes d'euros
  var m_prix_revente = 2400
  var m_prix_amelioration = 0
  var m_amelioration_bde : Option[Membre_bde_usine] = None
  var m_cadence_de_tir = president.get_nb_caract_mini_steps*15
  var m_projectile : Projectile_usine = proj_Coeur_usine
  var m_strategie : Strategie_bde = Strategie_premier()
  var m_portee = 0.
}
class Membre_bde_Bouclette(bde_usine:Membre_bde_usine,coord:(Int,Int))
    extends Membre_bde(bde_usine,coord)
{}


// ____ Cthulu ____
object Membre_bde_Chtulu_usine extends Membre_bde_usine
{
  def apply(coord : (Int,Int)) = new Membre_bde_Chtulu(this,coord)
  var m_nom = "membre_bde_Chtulu"
  var m_pretty_nom = "Chtulu"
  val m_description = "Le dieu attaque tous ceux qui l'approchent"
  var m_prix = 8000 // Prix en centimes d'euros
  var m_prix_revente = 4800
  var m_prix_amelioration = 20000
  var m_amelioration_bde : Option[Membre_bde_usine] = Some(Membre_bde_Pasta_usine)
  var m_cadence_de_tir = president.get_nb_caract_mini_steps*2
  var m_projectile : Projectile_usine = proj_Tentacule_usine
  var m_strategie : Strategie_bde = Strategie_tous()
  var m_portee = 1.
}
class Membre_bde_Chtulu(bde_usine:Membre_bde_usine,coord:(Int,Int))
    extends Membre_bde(bde_usine,coord)
{}




// ____ Pastafarisme ____
object Membre_bde_Pasta_usine extends Membre_bde_usine
{
  def apply(coord : (Int,Int)) = new Membre_bde_Pasta(this,coord)
  var m_nom = "membre_bde_pasta"
  var m_pretty_nom = "Monstre Spaghettis Volants"
  val m_description = "Le dieu attaque tous ceux qui l'approchent, encore plus fort"
  var m_prix = 12000 // Prix en centimes d'euros
  var m_prix_revente = 7200
  var m_prix_amelioration = 0
  var m_amelioration_bde : Option[Membre_bde_usine] = None
  var m_cadence_de_tir = president.get_nb_caract_mini_steps*2
  var m_projectile : Projectile_usine = proj_Tentacule_usine
  var m_strategie : Strategie_bde = Strategie_tous()
  var m_portee = 2.
}
class Membre_bde_Pasta(bde_usine:Membre_bde_usine,coord:(Int,Int))
    extends Membre_bde(bde_usine,coord)
{}


